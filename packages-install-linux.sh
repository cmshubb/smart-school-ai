#!/bin/bash
pip3 install flask
pip3 install cmake 
pip3 install dlib --verbose
pip3 install face_recognition --verbose
pip3 install imutils
pip3 install opencv-python
pip3 install opencv-contrib-python
pip3 install python-dotenv
pip3 install flask_wtf
pip3 install flask-migrate
pip3 install flask-script
pip3 install active_alchemy
pip3 install scipy
pip3 install jsonpickle
pip3 install Flask-Babel
