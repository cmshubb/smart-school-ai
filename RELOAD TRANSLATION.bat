@echo OFF
pybabel extract -F babel.cfg -o messages.pot .
pybabel init -i messages.pot -d facereko_webapp/translations -l ar
pybabel compile -d facereko_webapp/translations
PAUSE