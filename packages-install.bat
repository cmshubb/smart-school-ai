@echo OFF
pip install flask
pip install cmake 
pip install dlib --verbose
pip install face_recognition --verbose
pip install imutils
pip install opencv-python
pip install opencv-contrib-python
pip install python-dotenv
pip install flask_wtf
pip install flask-migrate
pip install flask-script
pip install active_alchemy
pip install scipy
pip install jsonpickle
pip install Flask-Babel
PAUSE
