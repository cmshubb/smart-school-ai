@echo OFF
pybabel extract -F babel.cfg -o messages.pot .
pybabel update -i messages.pot -d facereko_webapp/translations
pybabel compile -d facereko_webapp/translations
PAUSE