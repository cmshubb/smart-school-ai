$(document).ready(() => {
  var softAlarm = document.getElementById("SoftAlarm");
  var hardAlarm = document.getElementById("HardAlarm");
  var isPlay = true;
  let counter = 1;
  // HIDE MESSAGES INITIALLY
  $("#Nothing").hide();
  $("#Known").hide();
  $("#Unknown").hide();
  $("#Blacklist").hide();
  $("#Mute").hide();

  // PLAY/MUTE BUTTON CLICK
  $("#PlayMuteBtn").click(() => {
    isPlay = !isPlay;
    if (isPlay) {
      $("#Play").show();
      $("#Mute").hide();
    } else {
      softAlarm.pause();
      $("#Play").hide();
      $("#Mute").show();
    }
  });

  // CLEAR LOG BUTTON CLICK
  $("#ClearLogBtn").click(() => {
    $("#Result").find("tr").remove();
    counter = 0;
  });

  // STEOP STREAM BUTTON CLICK
  //   $("#StopStreamBtn").click(() => {
  //     $.ajax({
  //       url: "/livefeed/release_resources",
  //       success: function (data) {
  //         console.log(data);
  //       },
  //     });
  //   });

  (function () {
    // do some stuff
    $.ajax({
      url: "/en/video/result",
      success: function (data) {
        // If the result is empty then no faces were detected
        if (data.result && data.result.length == 0) {
          $("#Known").hide();
          $("#Nothing").show();
          $("#Unknown").hide();
          $("#Blacklist").hide();
        }
        let countUnknown = 0;
        let countBlacklist = 0;
        // Loop on all result items
        data.result.map((res, index) => {
          let statusBG = "alert-danger";
          if (res.status == "whitelist") {
            statusBG = "alert-success";
          } else if (res.name == "Unknown") {
            statusBG = "alert-warning";
          }
          // The result has Unknown name
          if (res.name == "Unknown") {
            countUnknown++;
            $("#Known").hide();
            $("#Nothing").hide();
            $("#Unknown").show();
            //$("#Blacklist").hide();
            if (isPlay) {
              softAlarm.play();
            }
          }
          if (res.status == "blacklist") {
            countBlacklist++;
            $("#Known").hide();
            $("#Nothing").hide();
            //$("#Unknown").hide();
            $("#Blacklist").show();
            if (isPlay) {
              hardAlarm.play();
            }
          }
          if (!$("#Result").find("#" + res.id).length) {
            $("#Result").append(
              '<tr class="alert ' +
                statusBG +
                '" id="' +
                res.id +
                '" class="col-sm-2">' +
                "<td>" +
                counter++ +
                "</td>" +
                "<td>" +
                "<h4>" +
                res.name +
                "</h4>" +
                "</td>" +
                "<td>" +
                '<img width="120" src="/en/persons/image/' +
                res.image +
                '" />' +
                "</td>" +
                "<td>" +
                new Date().today() +
                " " +
                new Date().timeNow() +
                "</td>" +
                "</tr>"
            );
          }
        });
        if (countUnknown == 0 && countBlacklist == 0 && data.result.length > 0) {
          $("#Unknown").hide();
          $("#Nothing").hide();
          $("#Known").show();
          $("#Blacklist").hide();
        }
      },
    });
    setTimeout(arguments.callee, 500);
  })();

  $(window).on("beforeunload", function (e) {
    $.ajax({
      url: "/en/video/release_resources",
      success: function (data) {
        console.log(data);
      },
    });
  });
});
