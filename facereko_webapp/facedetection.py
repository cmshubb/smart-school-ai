import face_recognition
import cv2
import numpy as np
from . import dataset
from datetime import datetime
import imutils
from imutils.video import FPS
from . import db
from .models import Persons, Faces
import os
from uuid import uuid1

TOLERANCE = 0.55  # 0.5 to 0.6 is acceptable based on camera setup
MODEL = "hog"  # cnn (GPU) Or hog (CPU)
UNKNOWN = "Unknown"
DATASET_PATH = os.path.join("./facereko_webapp/dataset")
WIDTH_SCALE = 320

def get_name_from_faceid(file_id):
    # If the sent file_id is "Unknown" then add it to the database
    # if file_id == UNKNOWN:
    #     # Generate a new file_id
    #     new_file_id = str(uuid1())
    #     file_name = new_file_id + '.png'
    #     encoded_face_file = new_file_id + '.json'
    #     file_path = os.path.join(DATASET_PATH, file_name)
    #     # Save image to dataset folder
    #     cv2.imwrite(file_path, face_frame)
    #     # encode json fie
    #     encode_face_result = dataset.encode_file(DATASET_PATH, file_name, new_file_id)
    #     new_person = Persons(name=UNKNOWN, status='whitelist')
    #     new_person.faces.append(Faces(face_image_file=file_name, face_encoding_file=encoded_face_file, file_id=new_file_id))
    #     db.session.add(new_person)
    #     db.session.commit()  # save changes to database
    # else:
    face_matching = Faces.query().filter(Faces.file_id == file_id).first()
    box_color = (0, 230, 230)  # yellow
    if face_matching:
        box_color = (0, 255, 0)  # green
        if face_matching.person.status == 'blacklist':
            box_color = (0, 0, 255)  # red
        return face_matching.person.name, box_color
    return UNKNOWN, box_color


class FaceDetection:
    def __init__(self, width_scale=WIDTH_SCALE):
        # Initialize some variables
        self.face_locations = []
        self.face_encodings = []
        self.face_names = []
        self.face_ids = []
        self.box_colors = []  # the  box colors of the faces\
        self.face_match_percentages = []
        self.process_this_frame = True
        encodings = dataset.get_face_encodings_dataset()
        self.known_face_encodings = encodings["encodings"]
        self.known_face_names = encodings["labels"]
        self.width_scale = width_scale
        # self.fps = FPS().start()

    def detect_faces(self, frame, isStream=False):
        if self.width_scale:
            frame = imutils.resize(frame, width=self.width_scale)
        # frame = cv2.resize(frame, None, fx=self.scale, fy=self.scale)
        # Convert the image from BGR color (which OpenCV uses) to RGB color (which face_recognition uses)
        rgb_frame = frame
        if isStream:
            rgb_frame =np.ascontiguousarray(rgb_frame[:, :, ::-1])

        # Only process every other frame of video to save time
        if self.process_this_frame:
            # Find all the faces and face enqcodings in the frame of video
            # !!! TO DO: APPLY MULTIPROCESSING TO IMPROVE CPU BOUND PROCESS OF FACE LOCATIONS
            self.face_locations = face_recognition.face_locations(rgb_frame, model=MODEL)
            # !!! TO DO: APPLY MULTIPROCESSING TO IMPROVE CPU BOUND PROCESS OF FACE ENCODINGS
            self.face_encodings = face_recognition.face_encodings(rgb_frame, self.face_locations)

            # clear the arrays
            self.face_names = []
            self.face_ids = []
            self.box_colors = []
            self.face_match_percentages = []
            for face_encoding in self.face_encodings:
                # See if the face is a match for the known face(s)
                # !!! TO DO: APPLY MULTITHREADING OR MULTIPROCESSING TO PROCESS ALL FACES AT THE SAME TIME
                matches = face_recognition.compare_faces(self.known_face_encodings, face_encoding, tolerance=TOLERANCE)
                #print(f"MATCHES: {matches}")
                face_id = UNKNOWN
                # If a match was found in known_face_encodings, just use the first one.
                # if True in matches:
                #     first_match_index = matches.index(True)
                #     face_id = self.known_face_names[first_match_index]

                # Or instead, use the known face with the smallest distance to the new face
                face_distances = face_recognition.face_distance(self.known_face_encodings, face_encoding)
                #print(f"FACE NAMES: {self.known_face_names}")
                #print(f"FACE DISTANCES: {face_distances}")
                best_match_index = np.argmin(face_distances)
                print("[Info] face distances",face_distances)

                # calculate Percentage of Matches
                match_percentage = face_distances[best_match_index] / TOLERANCE
                match_percentage = match_percentage * 100
                match_percentage = "%" + str("{:.2f}".format(match_percentage))

                if matches[best_match_index]:
                    face_id = self.known_face_names[best_match_index]
                # append the face_id to the face_ids list
                self.face_ids.append(face_id)
                self.face_match_percentages.append(match_percentage)
                # query/generate from database the face name for the face_id
                # top, right, bottom, left = self.face_locations[0]
                # exp = 10
                # top = top-exp*2
                # left = left-exp*2
                # if top < 1:
                #     top = 1
                # if left < 1:
                #     left = 1
                # face_frame = frame[top:bottom + exp * 2, left:right + exp * 2]
                # get_name = generate_name_from_faceid(face_id, face_frame)
                get_name, box_color = get_name_from_faceid(face_id)
                # If the retrived name is Unknown then retrive again dataset
                # if get_name == UNKNOWN:
                #     encodings = dataset.get_face_encodings_dataset()
                #     self.known_face_encodings = encodings["encodings"]
                #     self.known_face_names = encodings["labels"]
                self.face_names.append(get_name)
                self.box_colors.append(box_color)

        self.process_this_frame = not self.process_this_frame
        # Display the results
        # !!! TO DO: IMPROVE DRAWING CV2 RESULT USING MULTIPROCESSING
        for (top, right, bottom, left), name, name_box_color in zip(self.face_locations, self.face_names, self.box_colors):
            # Draw a box around the face
            left = left - 25
            top = top - 25
            right = right + 25
            bottom = bottom + 25
            cv2.rectangle(frame, (left, top), (right, bottom), name_box_color, 4)
            # Draw a label with a name below the face
            # cv2.rectangle(frame, (left, bottom - 20), (right, bottom), name_box_color, cv2.FILLED)
            # font = cv2.FONT_HERSHEY_COMPLEX
            # cv2.putText(frame, name, (left + 6, bottom - 6), font, 0.75, (0, 0, 0), 1)
            #cv2.imwrite('records/' + name + '_' + datetime.now().strftime('%d%m%Y %H-%M') + '.jpg', frame)
        # self.fps.update()
        return frame, self.face_ids, self.face_match_percentages

