import cv2
from threading import Thread
from datetime import datetime


class VideoRecord():
    def __init__(self, frame_rate=25):
        # Default resolutions of the frame are obtained (system dependent)
        self.frame_width = int(self.video.get(3))
        self.frame_height = int(self.video.get(4))

        # Set up codec and output video settings
        now = datetime.now().strftime("%d%m%Y")
        self.codec = cv2.VideoWriter_fourcc('M', 'J', 'P', 'G')
        self.output_video = cv2.VideoWriter(
            'records/' + now + '.avi', self.codec, frame_rate, (self.frame_width, self.frame_height))

    def save_frame(self, frame):
        # Save obtained frame into video output file
        self.output_video.write(frame)
