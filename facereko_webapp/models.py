from . import db
from datetime import datetime


class Videos(db.Model):
    """Data model for videos."""
    __tablename__ = 'videos'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(300), nullable=False)
    thumbnail = db.Column(db.String(300), nullable=True)
    video_file = db.Column(db.String(300), nullable=False)
    file_id = db.Column(db.String(300), nullable=False)
    extension = db.Column(db.String(30), nullable=False)

    def __repr__(self):
        return '<Videos {}>'.format(self.name)


class Cameras(db.Model):
    """Data model for cameras."""
    __tablename__ = 'cameras'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(300), nullable=False)
    camera_type = db.Column(db.String(300), nullable=False)
    status = db.Column(db.String(300), nullable=False)
    description = db.Column(db.String(500), nullable=True)
    connection_string = db.Column(db.String(500), nullable=False)
    logs = db.relationship('Logs', backref='cameras', lazy=True)

    def __repr__(self):
        return '<Cameras {}>'.format(self.name)


class Departments(db.Model):
    """Data model for departments."""

    __tablename__ = 'departments'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(300), nullable=False)
    persons = db.relationship('Persons', backref='departments', lazy=True)

    def __repr__(self):
        return '<Departments {}>'.format(self.name)


class Persons(db.Model):
    """Data model for persons."""

    __tablename__ = 'persons'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(300), nullable=False)
    status = db.Column(db.String(300), nullable=True)
    civil_id = db.Column(db.String(14), index=True, nullable=True)
    civil_id_image_file = db.Column(db.String(300), nullable=False)
    date_of_birth = db.Column(db.DateTime, nullable=True)
    gender = db.Column(db.String(20), nullable=True)
    faces = db.relationship('Faces', backref='persons', lazy=True)
    logs = db.relationship('Logs', backref='persons', lazy=True)
    department_id = db.Column(db.Integer, db.ForeignKey(
        'departments.id'), nullable=True)

    def __repr__(self):
        return '<Persons {}>'.format(self.name)


class Faces(db.Model):
    """Data model for faces."""

    __tablename__ = 'faces'
    id = db.Column(db.Integer, primary_key=True)
    file_id = db.Column(db.String(300), nullable=False)
    face_image_file = db.Column(db.String(300), nullable=False)
    face_encoding_file = db.Column(db.String(300), nullable=False)
    person_id = db.Column(db.Integer, db.ForeignKey('persons.id'), nullable=False)
    person = db.relationship('Persons', back_populates="faces")

    def __repr__(self):
        return '<Faces {}>'.format(self.face_image_file)

class Cars(db.Model):
    """Data model for cars and thier plates."""

    __tablename__ = 'cars'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(300), nullable=True)
    status = db.Column(db.String(300), nullable=True)
    owner = db.Column(db.String(300), nullable=True)
    color = db.Column(db.String(300), nullable=True)
    plate_file_id = db.Column(db.String(300), nullable=False)
    plate_image_file = db.Column(db.String(300), nullable=False)
    plate_numbers = db.Column(db.String(300), nullable=False)
    plate_characters = db.Column(db.String(300), nullable=False)


class Logs(db.Model):
    """Data model for logs."""

    __tablename__ = 'logs'
    id = db.Column(db.Integer, primary_key=True)
    log_type = db.Column(db.String(300), nullable=True)
    log_message = db.Column(db.Text, nullable=True)
    frame_file = db.Column(db.String(300), nullable=True)
    camera_id = db.Column(db.Integer, db.ForeignKey(
        'cameras.id'), nullable=False)
    camera = db.relationship('Cameras', back_populates="logs")
    person_id = db.Column(db.Integer, db.ForeignKey(
        'persons.id'), nullable=True)
    person = db.relationship('Persons', back_populates="logs")
    match_percentage = db.Column(db.String(300), nullable=True)

    def __repr__(self):
        return '<Logs {}>'.format(self.face_image_file)
