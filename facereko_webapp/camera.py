import cv2
from threading import Thread
from datetime import datetime


class VideoCamera(object):
    def __init__(self, src):
        # capturing video
        self.video = cv2.VideoCapture(src)
        (self.grabbed, self.frame) = self.video.read()
        # store the start time, end time, and total number of frames
        # that were examined between the start and end intervals
        self.fps_start = None
        self.fps_end = None
        self.fps_numFrames = 0
        # initialize the variable used to indicate if the thread should be stopped
        self.stopped = False
        self.motion_detected = False
        # Initialize face detector

    def start(self):
        # start the thread to read frames from the video stream
        thread = Thread(target=self.update, args=())
        thread.setDaemon(True)
        thread.start()
        self.fps_start = datetime.now()
        return self

    def update(self):
        # keep looping infinitely until the thread is stopped
        while True:
            # if the thread indicator variable is set, stop the thread
            if self.stopped:
                return
            # otherwise, read the next frame from the stream
            (self.grabbed, self.frame) = self.video.read()
            # update the total collected frames so far
            self.fps_numFrames += 1
            # small_frame = cv2.resize(
            #     self.frame, (0, 0), fx=self.down_scale, fy=self.down_scale)

    def get_fps(self):
        self.fps_end = datetime.now()
        elapsed = (self.fps_end - self.fps_start).total_seconds()
        fps = 0
        if not self.fps_numFrames == 0:
            fps = self.fps_numFrames / elapsed
        return elapsed, fps

    def get_frame(self):
        # extracting frames
        return self.grabbed, self.frame

    def stop(self):
        # indicate that the thread should be stopped
        self.stopped = True

    def convert_frame_to_jpgbytes(self, frame):
        encoded, jpeg = cv2.imencode('.jpg', frame)
        return jpeg.tobytes()

    def __del__(self):
        # releasing camera
        self.stop()
        self.video.release()
