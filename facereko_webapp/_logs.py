import functools
from flask import (Flask,
                   Blueprint, flash, g, redirect, render_template, Response, request, jsonify, session, url_for,
                   send_from_directory, current_app
                   )

from . import db
from .models import Persons, Faces, Logs
from .facedetection import FaceDetection
from .datatables.tablebuilder import TableBuilder
from .persondetection import PersonDetection
from .camera import VideoCamera
import numpy as np
import os
from .validators import check_license
from flask_babel import _

bp = Blueprint('logs', __name__, url_prefix='/<lang_code>/logs')
table_builder = TableBuilder()


@bp.route('/')
def index():
    return check_license.check_expired('logs/index.html', title=_('Logs Archive'))


@bp.route("/load_table", methods=['GET'])
def load_table():
    data = table_builder.collect_data_serverside(request)
    return jsonify(data)


@bp.route('/delete_all')
def delete_all():
    try:
        logs = Logs.query().filter().all()
        for log in logs:
            # delete frame from folder
            frame_file_path = os.path.join(current_app.config["LOGS"], log.frame_file)
            if os.path.exists(frame_file_path):
                os.remove(frame_file_path)
            db.session.delete(log)
        db.session.commit()  # save changes to database
        flash(f'All logs were deleted successfully !', 'success')
    except:
        flash(f'Failed to delete person, try again later.', 'danger')
    return redirect(url_for('.index'))


@bp.route('/delete/')
@bp.route('/delete/<log_id>')
def delete(log_id):
    try:
        log = Logs.query().filter(Logs.id == log_id).first()
        # delete frame from folder
        frame_file_path = os.path.join(current_app.config["LOGS"], log.frame_file)
        if os.path.exists(frame_file_path):
            os.remove(frame_file_path)
        db.session.delete(log)
        db.session.commit()  # save changes to database
        flash(_(f'Log was deleted successfully'), 'success')
    except:
        flash(f'Failed to delete log, try again later.', 'danger')
    return redirect(url_for('.index'))


# Get Image from Folder DataSet
@bp.route('/frame/<filename>')
def send_frame(filename):
    return send_from_directory("logs", filename)


@bp.route('/today')
def today():
    return render_template('logs/today.html', title="Today's Activity")
