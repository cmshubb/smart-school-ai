from flask import (Flask, render_template, g, request,
                   redirect, url_for, current_app, abort)
# from flask_sqlalchemy import SQLAlchemy
from active_alchemy import ActiveAlchemy
from flask_migrate import Migrate, MigrateCommand
from flask_babel import Babel
from datetime import datetime
from .validators import check_license
# db = SQLAlchemy() //Disabled to use ActiveAlchemy instead, as it supports out of request context access to Database
db = ActiveAlchemy("mysql+pymysql://root@localhost/facereko_db", echo=False)
migrate = Migrate()

# create and configure the app
app = Flask(__name__, instance_relative_config=False)
app.debug = True
# set up babel
babel = Babel(app)


@babel.localeselector
def get_locale():
    if not g.get('lang_code', None):
        g.lang_code = request.accept_languages.best_match(
            app.config['LANGUAGES'])
    return g.lang_code


@app.url_defaults
def set_language_code(endpoint, values):
    if "lang_code" in values or not g.get("lang_code", None):
        return
    if app.url_map.is_endpoint_expecting(endpoint, "lang_code"):
        values["lang_code"] = g.lang_code


@app.url_value_preprocessor
def get_lang_code(endpoint, values):
    if values is not None:
        g.lang_code = values.pop("lang_code", None)


@app.before_request
def ensure_lang_support():
    lang_code = g.get("lang_code", None)
    if lang_code and lang_code not in app.config["LANGUAGES"]:
        abort(404)


@app.context_processor
def utility_processor():
    return dict(lang_code=babel.locale_selector_func)


def create_app():

    app.config.from_object('config.Config')
    db.init_app(app)
    migrate.init_app(app, db)

    with app.app_context():

        # HOME PAGE ROUTE
        @app.route("/")
        @app.route("/<lang_code>/")
        def index(lang_code=None):
            # check_license.check_expired()
            # # Check if exceeded time
            # now = datetime.now()
            # expiry_date = datetime(2020,11,15)
            # # Check if demo expired
            # if now > expiry_date:
            #     print("License expired. Please contact ITPS team for renewal.")
            #     return render_template('home/expired.html')
            return check_license.check_expired('home/index.html')

        @app.route("/<lang_code>/")
        @app.route('/comingsoon')
        def comingsoon(lang_code=None):
            return render_template('home/comingsoon.html')

        # YOU MUST CREATE AN EMPTY DATABASE IN MYSQL FIRST TO CREATE TABLES
        from .models import Persons, Faces, Departments, Cameras, Logs, Cars
        db.create_all()  # Create database tables for our data models

        # Global Variables
        @app.context_processor
        def context_processor():
            cameras = Cameras.query().all()
            return dict(cameras=cameras)

        # Blueprints
        from . import _livefeed
        app.register_blueprint(_livefeed.bp)

        from . import _video
        app.register_blueprint(_video.bp)

        from . import _persons
        app.register_blueprint(_persons.bp)

        from . import _api
        app.register_blueprint(_api.bp)

        from . import _logs
        app.register_blueprint(_logs.bp)

        from . import _cameras
        app.register_blueprint(_cameras.bp)

        from . import _reports
        app.register_blueprint(_reports.bp)

        app.add_url_rule('/', endpoint='index')

        return app