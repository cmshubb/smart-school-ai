from facereko_webapp.datatables.serverside.serverside_table import ServerSideTable
from facereko_webapp.datatables.serverside import table_schemas
from facereko_webapp.models import Logs
from flask import url_for
from flask_babel import lazy_gettext as _
from sqlalchemy import not_


def get_all_logs():
    result = []
    delete_string = _('Delete')
    logs = Logs.query().order_by(Logs.created_at.desc()).filter(not_(Logs.person_id == None)).all()
    for log in logs:
        status_class = 'badge-success' if log.log_type == 'whitelist' else 'badge-danger'
        status = f'<span class="badge font-20 {status_class}">{log.log_type}</span>'
        delete_action = url_for('logs.delete', log_id=log.id, lang_code='ar')
        frame_url = url_for('logs.send_frame', filename=log.frame_file)
        person_name = log.person.name if log.person else '-'
        result.append({
            'id': log.id,
            'person': person_name,
            'date': log.created_at.strftime('%d/%m/%Y %H:%M:%S'),
            'status': status,
            'frame': f'<a href="{frame_url}" data-fancybox data-caption="{person_name} | {log.log_type}"><img width="200" src="{frame_url}"/></a>',
            'match':log.match_percentage,
            'action': f'<a class="btn btn-sm btn-link text-danger" href="{delete_action}"><i class="icon-Close"></i> {delete_string}</a>'
        })
    return result


class TableBuilder(object):

    def collect_data_clientside(self):
        return {'data': get_all_logs()}

    def collect_data_serverside(self, request):
        columns = table_schemas.SERVERSIDE_TABLE_COLUMNS
        return ServerSideTable(request, get_all_logs(), columns).output_result()
