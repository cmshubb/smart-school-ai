SERVERSIDE_TABLE_COLUMNS = [
    {
        "data_name": "id",
        "column_name": "#",
        "default": 0,
        "order": 1,
        "searchable": True
    },
    {
        "data_name": "person",
        "column_name": "Person",
        "default": "-",
        "order": 2,
        "searchable": True
    },
    {
        "data_name": "date",
        "column_name": "Date / Time",
        "default": "-",
        "order": 3,
        "searchable": True
    },
    {
        "data_name": "status",
        "column_name": "Status",
        "default": "-",
        "order": 4,
        "searchable": True
    },
    {
        "data_name": "frame",
        "column_name": "Frame",
        "default": "",
        "order": 5,
        "searchable": True
    },
    {
        "data_name": "match",
        "column_name": "Match Percentage",
        "default": "",
        "order": 6,
        "searchable": False
    },
    {
        "data_name": "action",
        "column_name": "-",
        "default": "-",
        "order": 7,
        "searchable": False
    }
]
