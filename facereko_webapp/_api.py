from werkzeug.utils import secure_filename
import functools

from flask import (Flask,
                   Blueprint, flash, g, redirect, render_template, Response, request, jsonify, session, url_for,
                   send_from_directory, current_app
                   )
from werkzeug.security import check_password_hash, generate_password_hash
from . import dataset
from . import db
from .models import Persons, Faces
from .facedetection import FaceDetection
from .forms.persons import AddPersonForm
# from .libraries.license import LPReader, OCRandPreprocess, LPRProcessingService
from .libraries.ocr.card_processing_service import CardProcessingService
from .libraries.ocr import national_id_reader
import cv2
import numpy as np
import jsonpickle
from datetime import datetime, date
import os
import pprint


bp = Blueprint('api', __name__, url_prefix='/api')

@bp.route('/ocr_license_plate', methods=['POST'])
def ocr_license_plate():
    plate_image = request.files["plate_image"]
    # store the image in temp directory
    file_name, file_path = LPRProcessingService.store_image(plate_image)
    # detect the license plate
    # crop and resize 
    # ocr processing for image

@bp.route('/ocr_national_id', methods=['POST'])
def ocr_national_id():
    processing_service = CardProcessingService()
    print('[INFO] Starting OCR for National ID...')
    # get the national id files from the request
    front_file = request.files["national_id_front"]
    # back_file = request.files["national_id_back"]
    # store original images to temp
    front_file_name, front_file_path = processing_service.store_image(
        '_front', front_file)
    # back_file_name, back_file_path = processing_service.store_image(
    #     'back', back_file)
    if not front_file_path:
        print(front_file_path)
        return Response("Error cannot detect edges", status=400)
    # preprocess the images and save to dataset
    front_processed_file_name, front_processed_file_path = processing_service.preprocess_image(
        front_file_name, front_file_path)
    # back_processed_file_path = processing_service.preprocess_image(
    #     back_file_name, back_file_path)
    # read the data from id front and back
    national_id_reader.front_read(front_processed_file_path)
    # national_id_reader.back_read(back_processed_file_path)
    # create the response data
    response = {
        'file_name': front_processed_file_name,
        'id': national_id_reader.IDNumber,
        'name': national_id_reader.name,
        'date': national_id_reader.BDate
    }
    response_pickled = jsonpickle.encode(response)

    return Response(response=response_pickled, status=200, mimetype="application/json")

@bp.route('/detect_faces', methods=['POST'])
def detect_faces():
    print("INFO: Starting face detection process...")
    detect = FaceDetection()
    # 1) Get the submitted image from the request
    r = request
    # convert string of image data to uint8
    nparr = np.fromstring(r.data, np.uint8)
    # decode image
    img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
    print("INFO: Image encoded to numpy array from request...")

    # do some fancy processing here....
    img, face_names = detect.detect_faces(img, False)
    # 2) Process image and detect if has any faces or not
    # 3) Return json response with result
    # build a response dict to send back to client
    status = "Known"
    message = "Info: Known faces were detected"
    if "Unknown" in face_names:
        message = "Warning: Unknown Person Detected !"
        status = 'Unknown'
    response = {'names': face_names, 'message': message, 'status': status}
    # encode response using jsonpickle
    response_pickled = jsonpickle.encode(response)

    return Response(response=response_pickled, status=200, mimetype="application/json")


@bp.route('/persons/all', methods=['GET'])
def allpersons():
    result = []
    persons = Persons.query().all()
    for person in persons:
        result.append(create_person_view(person))
    return jsonify(result)


@bp.route('/persons/add', methods=['POST'])
def addperson():
    form = AddPersonForm()
    ### POST REQUEST ###
    if form.is_submitted():
        # validate that submitted data is valid
        # if not form.validate():
        #     return jsonify(f'Please make sure you entered all required data correctly !')
        # validate that there is and uploaded image
        if not form.face_image.data:
            return jsonify(f'Please upload an image for the person !')
        # generate unique file id and save it to uploads
        image_file = request.files[form.face_image.name]  # get the image file
        extension = os.path.splitext(image_file.filename)[1]
        file_id = str(uuid1())
        file_name = file_id + extension
        encoded_face_file = file_id + '.json'
        # check if uploaded file is an allowed image file
        if not allowed_image(image_file.filename):
            return jsonify(f'Please upload a valid image file !')
        # save image to uploads directory
        image_file.save(os.path.join(current_app.config["DATASET"], file_name))
        encode_face_result = dataset.encode_file(
            current_app.config["DATASET"], file_name, file_id)
        if not encode_face_result:
            return jsonify(f'Uploaded image has no valid faces, please try again !')

        # add person to the database
        new_person = Persons(name=form.name.data, status=form.status.data, civil_id=form.civil_id.data,
                             date_of_birth=form.date_of_birth.data, gender=form.gender.data)
        new_person.faces.append(Faces(
            face_image_file=file_name, face_encoding_file=encoded_face_file, file_id=file_id))
        db.session.add(new_person)
        db.session.commit()  # save changes to database
        return jsonify(f'{form.name.data} was added successfully !')

####### HELPER FUNCTIONS ########


def create_person_view(person):
    # Create person view to display in template
    face = person.faces[0]
    date_of_birth = None
    if person.date_of_birth:
        date_of_birth = person.date_of_birth.strftime('%d/%m/%Y')
    person_view = {
        'id': person.id,
        'name': person.name,
        'status': person.status,
        'civil_id': person.civil_id,
        'date_of_birth': date_of_birth,
        'age': calculate_age(person.date_of_birth),
        'gender': person.gender,
        'image': '/image/' + face.face_image_file,
    }
    return person_view


def allowed_image(filename):
    # Check Image Extention "JPEG", "JPG", "PNG", "GIF"
    if not "." in filename:
        return False

    ext = filename.rsplit(".", 1)[1]

    if ext.upper() in current_app.config["ALLOWED_IMAGE_EXTENSIONS"]:
        return True
    else:
        return False


def calculate_age(born):
    # calculate age from datetime
    if born:
        today = date.today()
        return today.year - born.year - ((today.month, today.day) < (born.month, born.day))
    return None
####### HELPER FUNCTIONS ########
