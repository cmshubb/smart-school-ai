��    �      t              �     �     �     �     �     	     	  
   "	     -	  
   6	  
   A	     L	     Q	     ]	     y	     �	     �	     �	     �	     �	     �	  !   �	     �	     
     
     
      
     )
     2
     >
  	   L
     V
     \
  *   h
     �
     �
     �
     �
     �
  	   �
     �
     �
               "     0     9     >     J     W     c  
   q     |     �  +   �  .   �  +   �  -     C   A  5   �     �     �     �     �     �        	                       $     +     G  	   K     U     ^     {     �     �  .  �     �     �     �  
   �     �     �     �     
     #     1     B     J     Q     c     k     }     �  3   �     �     �     �     �       1   #  )   U       a   �  2   �  ;   #     _     h     o     �  	   �      �  w   �  1   /     a     r     y     �     �     �     �     �  E   �  	             ,  
   B     M     ^  
   m  	   x     �     �  4   �     �     �  �  �     �     �     �     �  
             -     E     W     k     }     �  1   �     �     �     �  "   �  1     1   K  
   }  *   �  "   �     �     �  
             $     <     X     s  
   �     �  3   �     �     �               =     W     t     {     �     �     �     �  
   �     �     �          *     @  
   Z     e  G   t  R   �  ?     E   O  [   �  C   �     5     T     j     {     �  
   �     �  
   �     �     �     �  *   �  ,        5     E  !   W     y     �     �  �  �     �     �  
   �     �  
   �     �  1   �  +         =      P      h   
   u      �      �   &   �      �   0   �   /   !     N!     h!     z!     �!  8   �!  M   �!  h    "     �"  Y   �"  A   �"  ]   9#     �#     �#  $   �#     �#     �#  "   �#  �   $  4   �$     �$     %     %  2   1%     d%     v%     �%     �%  7   �%     �%     �%  !   &     8&     N&     j&     �&     �&  
   �&     �&     �&     �&     �&   (Live) (Processing) 128 facial features. Account Setting Active Add
          Person Add Camera Add Face Add Face - Add Person Age: All Persons Analyse saved video content Arabic Archive Matching Back Back To Persons Back To Videos Archive Back to home Blacklisted Blacklisted Person was detected ! Blacklisted: Camera Type Cameras Cancel Civil ID Civil Id Civil Id... Clear All Log Clear Log Close Coming Soon Complete Security Solution With Confidence Connection String Connection String... Date Of Birth Date Of Birth... Date Of Birth: Date/Time Delete Delete Person Description Description... Detection Log Disabled Edit Edit Camera Edit Details Edit Person Edit Person - Edit Video Enabled English Enter details of the camera you want to add Enter details of the camera you want to update Enter details of the person you want to add Enter details of the video you want to update Ex. Entry Door Camera, Exit Door Camera, Hall Camera, PC Web Camera Extraced from uploaded image of national id using OCR Faces Registered For Person: Faces Registered: Features Female Frame Gender Gender... Gender: Home Image Indoor Known Person was detected ! LPR Live Feed Log Type Log was deleted successfully Logout Logs Logs Archive Lyzer AI is an artificial intelligence-based face and objects recognition platform that can be easily integrated with your IT tools and hardware. The platform can perform face recognition in real-time from video streams and photos
            using a state of the art deep neural network that considers Male Match Percentage Mute My Profile Name Name... National ID (Front) No faces were detected ! Notificaitons Object Detection Outdoor Person Person Face Image Persons Play / Mute Alarm Process Video Real-time video feed processing Release Candidate Version 0.9, Expires On 1/12/2021 Save Changes Save Person Search Search By Image Search By Person ID Search results found from extraced ID using OCR:  Secure Deployments on Cloud or On-Premise Select File... Select the face image you would like to upload for the
                    person (JPG, PNG, GIF) Select the image you want to upload for the person Select the video you would like to add to the video library Settings Sound: Static Photo analysis Status Status... Supports RTSP/IP/HTTP Camera’s The platform uses machine learning to continuously learn and improve its detection accuracy. The platform also supports This Features is a Work in progress. Stay Tuned ! Today's Activity Total: Unknown Unknown Person was detected ! Unknown: Upload File Upload Image Upload Video Upload a photo and use it to search for a person by their national id Uploaded: Video Archive Video File (mp4, avi) Video Name Video Processing Videos Archive Web Camera Whitelist Whitelisted Whitelisted: for gate
            intrusion detections of plates. ip cam: web cam: Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2021-03-15 15:07+0200
PO-Revision-Date: 2020-06-07 16:38+0200
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: ar
Language-Team: ar <LL@li.org>
Plural-Forms: nplurals=6; plural=(n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=0 && n%100<=2 ? 4 : 5)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.8.0
 (مباشر) (معالجة)  .128 ملمح للوجه إعدادات الحساب تشغيل اضف شخص إضافة كاميرا إضافة وجه إضافة وجه - إضافة شخص السن كل الأشخاص تحليل محتوى الفيديو المخزن عربي مطابقة السجل رجوع العودة إلى الأشخاص العودة إلى مكتبة الفديوهات الرجوع إلى الشاشة الرئيسية محظور تم العصور على شخص محظور قائمة غير مصرح لها  نوع الكاميرا الكاميرات إلغاء الرقم القومى الرقم القومى الرقم القومى ... حذف كل السجلات مسح السجل إغلاق قريباً الحل الأمنى الكامل مع الثقة  رابط الإتصال رابط الإتصال ... تاريخ الميلاد تاريخ الميلاد ... تاريخ الميلاد التاريخ /  الوقت حذف حذف الشخص التفاصيل الوصف ... سجل الكشف غير مُفعل تعديل تعديل الكاميرا تعديل البيانات تعديل الشخص تعديل الشخص تعديل الفيديو مُفعل انجليزي أدخل بيانات الكاميرا التى تريد إضافتها أدخل بيانات الكاميرا التى تريد التعديل عليها أدخل بيانات الشخص الذى تريد إضافته أدخل بيانات الفيديو التى تريد تعديلها مثل: كاميراالدخول, كاميراالخروج,الكاميراالرئيسية تستخرج من بطاقه الرقم القومي المحمله وجوه مسجلة للشخص وجوه مُسجلة المميزات أنثى الصورة النوع النوع ... النوع الرئيسية صورة داخلية تم العثور على شخص معروف  التعرف على لوحة الترخيص بث مباشر نوع السجل تم حذف السجل بنجاح تسجيل خروج السجلات أرشيف السجلات هو نظام التعرف على الوجوه والأشياء مبنى على الذكاء الإصطناعى Lyzer AIمنصة عمل يمكن دمجها بسهولة مع الأدوات التكنولوجية والأجهزةمنصة عمل يمكن من خلالها التعرف على الوجوه لحظياً خلال عملة البث المباشر للفيديووكذلك الصور من خلال حالة الشبكات العصبية العميقة ذكر نسبة التطابق إيقاف ملفى الشخصى الإسم الإسم ... بطاقه الرقم القومي (اماميه) لم يتم التعرف على أى وجه الإشعارات كشف الكائنات خارجية الشخص صورة وجه الشخص الأشخاص تشغيل / إيقاف الإنذار معالجة الفيديو معالجة حية للفيديوالمباشر الاصدار رقم 0.9, ينتهي 1/04/2021 حفظ التعديلات حفظ الشخص بحث بحث بالبطاقه :بحث عن طريق بطاقه الرقم القومي نتائج البحث التي وجدت عن طريق الرقم القومي عمليات نشر آمنى على السحابة الإلكترونية أو فى مكان العمل  إختر ملف ... إختر صورة الوجه التى تريد تحميلها للشخص (JPG, PNG, GIF)  إختر الصورة التى تريد تحميلها للشخص إختر الفيديو الذى تريد إضافته إلى مكتبة الفيديوهات الإعدادات صوت تحليل الصور الثابتة الحالة الحالة ... دعم كاميرات RTSP/IP/HTTP نسنخدم المنصه تقنيه تعلم الاله و الذكاء الاصطناعي لكي تطور من ادائها  المنصه تدعم تقنيه  هذه المميزات قيد العمل. ترقب! النشاطات اليومية الإجمالى: غير معروف تم  العثور على شخص غير معروف غير معروف رفع الملف تحميل صورة تحميل فيديو قم بالبحث عن طريق صوره البطاقه تحميل: مكتبة الفيديوهات (mp4 , avi) ملف الفيديو إسم الفيديو معالجة الفيديو سجل الفيديو كاميرا ويب غير محظور مسموح قائمة مُصرح لها للتحقق من صحتها. كاميرا ip كاميرا ويب 