import functools
from flask import (Flask,
                   Blueprint, flash, g, redirect, render_template, Response, request, jsonify, session, url_for,
                   send_from_directory, current_app
                   )

from . import db
from .models import Persons, Faces, Logs
from .facedetection import FaceDetection
from .datatables.tablebuilder import TableBuilder
from .persondetection import PersonDetection
from .camera import VideoCamera
import numpy as np
import os
from .validators import check_license
from flask_babel import _


bp = Blueprint('reports', __name__, url_prefix='/<lang_code>/reports')

### ANALYSIS PAGE ###
@bp.route('/analysis/')
def analysis():
    return render_template('reports/analysis.html')