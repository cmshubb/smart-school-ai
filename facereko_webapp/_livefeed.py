import functools
from flask import (Flask,
                   Blueprint, flash, g, redirect, render_template, Response, request, jsonify, session, url_for,
                   send_from_directory, current_app)

from . import db
from .models import Persons, Faces, Cameras, Logs
from .facedetection import FaceDetection
from .camera import VideoCamera
from time import time, sleep
import numpy as np
import os
import cv2
from .libraries.motion_detection.motion_detection_model import MotionDetectionModel
from uuid import uuid1
from .validators import check_license
from flask_babel import _

bp = Blueprint('livefeed', __name__, url_prefix='/<lang_code>/livefeed')

UNKNOWN = "Unknown"
LOGS_FOLDER = os.path.join("./facereko_webapp/logs")
persons_face_ids = []
persons_list_result = []
video_streams = []
motion_detected = False


### STREAMS PAGE ###
@bp.route('/streams/')
def streams():
    reset_streams()
    cameras = Cameras.query().all()
    return check_license.check_expired('livefeed/streams.html', title=_('Streams'), cameras=cameras)

@bp.route('/livelog/')
def livelog():
    return check_license.check_expired('livefeed/livelog.html', title=_('Live Logs'))

### CAMERA PAGE ###
@bp.route('/camera/')
@bp.route('/camera/<camera_id>')
def camera(camera_id):
    camera = Cameras.query().filter(Cameras.id == camera_id).first()
    return check_license.check_expired('livefeed/camera.html', title=f'{camera.name} (Live)', camera=camera)

### CAMERA LIVE FEED ###
@bp.route('/camfeed/')
@bp.route('/camfeed/<camera_id>')
def camfeed(camera_id):
    sleep(1)
    detect = FaceDetection()
    camera = Cameras.query().filter(Cameras.id == camera_id).first()
    motion = MotionDetectionModel()
    connection_string = camera.connection_string
    if connection_string == '0':
        connection_string = int(connection_string)
        detect.width_scale = None
    video_stream = VideoCamera(connection_string)
    video_stream.start()
    video_streams.append(video_stream)
    return Response(generatefeed(video_stream, camera, detect, motion, True), mimetype='multipart/x-mixed-replace; boundary=frame')

#### AJAX REQUESTS ####
@bp.route('/result')
def result():
    return jsonify(result=persons_list_result, motion=motion_detected)

@bp.route('/release_resources')
def release_resources():
    reset_streams()
    return jsonify(result=True)
######################

def reset_streams():
    global video_streams
    global persons_face_ids
    global persons_list_result
    for stream in video_streams:
        stream.stop()
    persons_list_result.clear()
    persons_face_ids.clear()
    print('[INFO] ALL IS RELEASED')

def generatefeed(video_stream, camera, detect, motion, isStream):
    detection_log = []
    while True:
        grabbed, frame = video_stream.get_frame()
        if grabbed:
            # Get camera frame
            face_frame, face_ids, face_match_percentages= detect.detect_faces(frame, isStream)
            add_to_log(face_frame, camera.id, face_ids, detection_log,face_match_percentages)
            set_persons_list(face_ids)
            frame = face_frame
            frame_bytes = video_stream.convert_frame_to_jpgbytes(frame)
            yield (b'--frame\r\n'
                    b'Content-Type: image/jpeg\r\n\r\n' + frame_bytes + b'\r\n\r\n')


def add_to_log(frame, camera_id, face_ids, detection_log, face_match_percentages):
    # query the face_ids from database
    for file_id, match_percentage in zip(face_ids,face_match_percentages):
        status = UNKNOWN
        message = "Unknown person was detected"
        unique_id = str(uuid1())
        file_name = unique_id + '.jpg'
        person_id = None
        # query the face from database
        # !!! TO DO: THE QUERY SHOULD HAPPEN AFTER CHECKING IF THE file_id EXISTS IN detection_log[]
        face_matching = Faces.query().filter(Faces.file_id == file_id).first()
        # (Blacklisted, or Whitelisted)
        if face_matching:
            # set the details necessary if the face exists
            status = face_matching.person.status
            person_id = face_matching.person_id
            if status == 'blacklist':
                message = "Blacklisted person was detected"
            else:
                message = "Known person was detected"
            # query in the log to check if already been logged in the past minute
            result_exists = [
                x for x in detection_log if x['person_id'] == person_id]
            if result_exists:
                now = time()
                # check if it has been since 1 minute
                for person in result_exists:
                    time_elapsed = now - person['created']
                    # print(f'IT HAS BEEN: {time_elapsed}')
                    if time_elapsed < 59:
                        return None
        # (UNKNOWN)
        # !!! TO DO: do logic here for checking if same unknown person was detected

        # save frame to file
        file_path = os.path.join(LOGS_FOLDER, file_name)
        cv2.imwrite(file_path, frame)  # save frame to logs folder
        # save the detection to database
        log = Logs(log_type=status, log_message=message,
                   frame_file=file_name, camera_id=camera_id, person_id=person_id, match_percentage=match_percentage)
        # add to in-memory detection log
        detection_log.append({'person_id': log.person_id, 'created': time()})
        #print(f'DETECTION LOG: {detection_log}')
        db.session.add(log)
        db.session.commit()
        # save the frame to records folder by the detection id
    #print(f'Saved To Log: {camera_id} and faces: {face_ids}')


def set_persons_list(face_ids):
    unknown_person = {'id': -1, 'file_id': '',
                      'image': '', 'name': UNKNOWN, 'status': ''}
    # Get the no of tracked persons and number of detected faces in frame
    no_of_faces = len(face_ids)
    # If there were no faces detected then clear the face ids, and persons result list
    if not face_ids:
        persons_face_ids.clear()
        persons_list_result.clear()
    # Remove unknown person if no longer exists
    if UNKNOWN in persons_face_ids and not UNKNOWN in face_ids:
        persons_face_ids.remove(UNKNOWN)
        persons_list_result.remove(unknown_person)
    # Remove blacklisted person if no longer exists
    blacklist_result = [
        x for x in persons_list_result if x['status'] == 'blacklist']
    for blacklisted_person in blacklist_result:
        if not blacklisted_person['file_id'] in face_ids:
            persons_face_ids.remove(blacklisted_person['file_id'])
            persons_list_result.remove(blacklisted_person)
    # If there are faces detected then loop on thier ids
    for file_id in face_ids:
        # Check if the person's id was already added to the list, to avoid doing again a query
        if not file_id in persons_face_ids:
            persons_face_ids.append(file_id)
            face_matching = Faces.query().filter(Faces.file_id == file_id).first()
            if face_matching:
                person_view = {
                    'id': face_matching.person.id,
                    'file_id': face_matching.file_id,
                    'image': face_matching.face_image_file,
                    'name': face_matching.person.name,
                    'status': face_matching.person.status
                }
                persons_list_result.append(person_view)
            elif file_id == UNKNOWN:
                persons_list_result.append(unknown_person)
