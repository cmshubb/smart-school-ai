from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, DateTimeField, RadioField, SubmitField, FileField, HiddenField
from wtforms.validators import DataRequired, Length
from flask_babel import lazy_gettext as _


class AddPersonForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()], render_kw={
                       "placeholder": _("Name...")})

    status = SelectField('Status', choices=[
                         ('blacklist', _('Blacklisted')), ('whitelist', _('Whitelisted'))], validators=[DataRequired()], render_kw={"placeholder": _("Status...")})

    civil_id = StringField('Civil Id', render_kw={
                           "placeholder": _("Civil Id...")})

    date_of_birth = DateTimeField('Date Of Birth', format='%d-%m-%Y', render_kw={
        "placeholder": _("Date Of Birth...")})

    gender = SelectField(
        'Gender', choices=[('male', _('Male')), ('female', _('Female'))], render_kw={
            "placeholder": _("Gender...")})

    face_image = FileField('Face Image', render_kw={"placeholder": _("Select File..."),
                                                    "data_allowed_file_extensions": "jpg jpeg png gif"})
                                                    
    national_id_front_image = FileField('National ID (Front)', render_kw={"placeholder": _("Select File..."),
                                                    "data_allowed_file_extensions": "jpg jpeg png gif"})

    national_id_filename = HiddenField('National ID (Image File)')
                                 
    submit = SubmitField(_('Save Person'))


class EditPersonForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()], render_kw={
                       "placeholder": _("Name...")})

    status = SelectField('Status', choices=[
                         ('blacklist', _('Blacklisted')), ('whitelist', _('Whitelisted'))], validators=[DataRequired()], render_kw={"placeholder": _("Status...")})

    civil_id = StringField('Civil Id', render_kw={
                           "placeholder": _("Civil Id...")})

    date_of_birth = DateTimeField('Date Of Birth', format='%d-%m-%Y', render_kw={
        "placeholder": _("Date Of Birth...")})

    gender = SelectField(
        'Gender', choices=[('male', _('Male')), ('female', _('Female'))], render_kw={
            "placeholder": _("Gender...")})

    submit = SubmitField(_('Save Changes'))


class AddFaceForm(FlaskForm):
    face_image = FileField('Face Image', render_kw={"placeholder": _("Select File..."),
                                                    "data_allowed_file_extensions": "jpg jpeg png gif",
                                                    "data_height": "500"})

    submit = SubmitField('Add Face')


class SearchByCivilIdImage(FlaskForm):
    image = FileField('National ID', render_kw={"placeholder": _("Select File..."),
                                                    "data_allowed_file_extensions": "jpg jpeg png gif",
                                                    "data_height": "250"})

    submit = SubmitField(_('Search'))
