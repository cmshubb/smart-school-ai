from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, DateTimeField, RadioField, SubmitField, FileField, HiddenField
from wtforms.validators import DataRequired, Length
from flask_babel import lazy_gettext as _


class AddCarForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()], render_kw={
                       "placeholder": _("Name...")})

    status = SelectField('Status', choices=[
                         ('blacklist', _('Blacklisted')), ('whitelist', _('Whitelisted'))], validators=[DataRequired()], render_kw={"placeholder": _("Status...")})

    color = StringField('Color', render_kw={"placeholder": _("Color...")})

    owner = StringField('Owner', render_kw={"placeholder": _("Owner...")})

    plate_number = StringField('Plate Number', render_kw={"placeholder": _("Plate Numbers...")})

    plate_chars = StringField('Plate Chars', render_kw={"placeholder": _("Plate Chars...")})

    plate_image = FileField('Plate ID', render_kw={"placeholder": _("Select File..."),
                                                           "data_allowed_file_extensions": "jpg jpeg png gif"})

    plate_filename = HiddenField('Plate ID (Image File Path)')

    submit = SubmitField(_('Save Car'))


class EditCarForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()], render_kw={
                       "placeholder": _("Name...")})

    status = SelectField('Status', choices=[
                         ('blacklist', _('Blacklisted')), ('whitelist', _('Whitelisted'))], validators=[DataRequired()], render_kw={"placeholder": _("Status...")})

    color = StringField('Color', render_kw={"placeholder": _("Color...")})

    owner = StringField('Owner', render_kw={"placeholder": _("Color...")})

    submit = SubmitField(_('Save Changes'))


class SearchByPlateImage(FlaskForm):
    image = FileField('Plate Image', render_kw={"placeholder": _("Select File..."),
                                                "data_allowed_file_extensions": "jpg jpeg png gif"})

    submit = SubmitField(_('Search'))
