from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, DateTimeField, RadioField, SubmitField, FileField, TextAreaField
from wtforms.validators import DataRequired, Length
from flask_babel import lazy_gettext as _


class AddCameraForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()], render_kw={
                       "placeholder": _("Name...")})

    status = SelectField('Status', choices=[('enabled', _('Enabled')), ('disabled', _('Disabled'))], validators=[
                         DataRequired()], render_kw={"placeholder": _("Status...")})

    camera_type = SelectField('Camera Type', choices=[('indoor', _('Indoor')), ('outdoor', _('Outdoor')), (
        'webcam', _('Web Camera'))], validators=[DataRequired()], render_kw={"placeholder": _("Status...")})

    connection_string = StringField('Connection String',  validators=[
                                    DataRequired()], render_kw={"placeholder": _("Connection String...")})

    description = TextAreaField('Description', render_kw={
                                "placeholder": _("Description...")})

    submit = SubmitField('Save Camera')


class EditCameraForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()], render_kw={
                       "placeholder": _("Name...")})

    status = SelectField('Status', choices=[('enabled', _('Enabled')), ('disabled', _('Disabled'))], validators=[
                         DataRequired()], render_kw={"placeholder": _("Status...")})

    camera_type = SelectField('Camera Type', choices=[('indoor', _('Indoor')), ('outdoor', _('Outdoor')), (
        'webcam', _('Web Camera'))], validators=[DataRequired()], render_kw={"placeholder": _("Status...")})

    connection_string = StringField('Connection String',  validators=[
                                    DataRequired()], render_kw={"placeholder": _("Connection String...")})

    description = TextAreaField('Description', render_kw={
                                "placeholder": _("Description...")})

    submit = SubmitField('Save Changes')
