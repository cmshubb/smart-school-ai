from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, DateTimeField, RadioField, SubmitField, FileField, TextAreaField
from wtforms.validators import DataRequired, Length
from flask_babel import lazy_gettext as _


class UploadVideoForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()], render_kw={
                       "placeholder": _("Name...")})

    video_file = FileField('Video File', render_kw={"placeholder": _("Select File..."),
                                                    "data_allowed_file_extensions": "avi mp4"})
    submit = SubmitField(_('Upload File'))


class EditVideoForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()], render_kw={
                       "placeholder": _("Name...")})

    submit = SubmitField(_('Save Changes'))
