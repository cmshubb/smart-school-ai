import functools
from flask import (Flask,
                   Blueprint, flash, g, redirect, render_template, Response, request, jsonify, session, url_for,
                   send_from_directory, current_app, abort
                   )
from flask_babel import _
from . import db
from .models import Cameras
from .forms.cameras import AddCameraForm, EditCameraForm
bp = Blueprint('cameras', __name__, url_prefix='/<lang_code>/cameras')


@bp.route('/')
def index():
    cameras = Cameras.query().all()
    return render_template('cameras/index.html', title=_('Cameras'), cameras=cameras, total_cameras=len(cameras))


@bp.route('/add', methods=('GET', 'POST'))
def add():
    form = AddCameraForm()
    ## POST REQUEST ##
    if form.is_submitted():
        # validate that submitted data is valid
        if not form.validate():
            flash(f'Please make sure you entered all required data correctly !', 'danger')
            return redirect(request.url)
        # add camera to database
        new_camera = Cameras(name=form.name.data,
                             connection_string=form.connection_string.data,
                             status=form.status.data,
                             camera_type=form.camera_type.data,
                             description=form.description.data)
        db.session.add(new_camera)
        db.session.commit()
        flash('Camera was added successfully', 'success')
        return redirect(url_for('.index'))
    ## GET REQUEST ##
    return render_template('cameras/add.html', title=_('Add Camera'), form=form)


@bp.route('/edit/', methods=('GET', 'POST'))
@bp.route('/edit/<camera_id>', methods=('GET', 'POST'))
def edit(camera_id):
    form = EditCameraForm()
    # get camera from database
    camera = Cameras.query().filter(Cameras.id == camera_id).first()
    ## POST REQUEST ##
    if form.is_submitted():
        # validate that submitted data is valid
        if not form.validate():
            flash(f'Please make sure you entered all required data correctly !', 'danger')
            return redirect(request.url)
        camera.name = form.name.data
        camera.connection_string = form.connection_string.data
        camera.status = form.status.data
        camera.camera_type = form.camera_type.data
        camera.description = form.description.data
        db.session.commit()  # save changes to database
        flash(f'{form.name.data} was updated successfully !', 'success')
        return redirect(url_for('.index', camera_id=camera_id))
    ## GET REQUEST ##
    # Set the form data
    form.name.data = camera.name
    form.status.data = camera.status
    form.camera_type.data = camera.camera_type
    form.connection_string.data = camera.connection_string
    form.description.data = camera.description
    return render_template('cameras/edit.html', title=_('Edit Camera'), camera_id=camera.id, form=form)


@bp.route('/delete/')
@bp.route('/delete/<camera_id>')
def delete(camera_id):
    try:
        camera = Cameras.query().filter(Cameras.id == camera_id).first()
        db.session.delete(camera)
        db.session.commit()  # save changes to database
        flash(f'Camera was deleted successfully', 'success')
    except:
        flash(f'Failed to delete camera, try again later.', 'danger')
    return redirect(url_for('.index'))
