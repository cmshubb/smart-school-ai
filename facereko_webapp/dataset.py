import os
import numpy as np
from json import JSONEncoder
import json
import cv2
import face_recognition
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
image_dir = os.path.join(BASE_DIR, "dataset")


class NumpyArrayEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return JSONEncoder.default(self, obj)
# Loop on all folders in Images

# This function is called once to extract encodings and save them to json files


def encode_file(root, file, label):
    path = os.path.join(root, file)
    #label = os.path.splitext(file)[0].replace(" ", "-").lower()
    filename = os.path.join(root, label + '.json')
    # Load a sample picture and learn how to recognize it.
    image = face_recognition.load_image_file(path)
    encode_face_result = face_recognition.face_encodings(image)
    if not encode_face_result:
        return False
    face_encoding = encode_face_result[0]
    print(
        '[Info] {name} was encoded successfully'.format(name=label))
    # Write encodings to file for faster startup later on
    with open(filename, "w") as write_file:
        json.dump(face_encoding, write_file, cls=NumpyArrayEncoder)
    return True

# NOT NEEDED ANYMORE
# def compute_face_encodings():
#     for root, dirs, files in os.walk(image_dir):
#         # Loop on all files in each folder
#         for file in files:
#             encode_file(root, file)


def get_face_encodings_dataset():
    # get all persons
    #persons = Persons.query.all()
    face_encodings = []
    labels = []
    for root, dirs, files in os.walk(image_dir):
        # Loop on all files in each folder
        for file in files:
            if file.endswith("json"):
                path = os.path.join(root, file)
                label = os.path.splitext(file)[0]
                with open(path, "r") as read_file:
                    decodedArray = json.load(read_file)
                    face_encoding = np.array(decodedArray)
                    face_encodings.append(face_encoding)
                    labels.append(label)

    print('[INFO] Loaded known faces successfully !')
    return {"encodings": face_encodings, "labels": labels}
