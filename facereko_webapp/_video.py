import functools
from multiprocessing import Process, current_process, Queue
from flask import (
    Blueprint, flash, g, redirect, render_template, Response, request, jsonify, session, url_for, current_app, send_from_directory, abort
)
from flask_babel import _
from . import db
from .models import Videos, Persons, Faces, Cameras
from .facedetection import FaceDetection
from .persondetection import PersonDetection
from .forms.video import UploadVideoForm, EditVideoForm
from .camera import VideoCamera
from uuid import uuid1
from os import path

bp = Blueprint('video', __name__, url_prefix='/<lang_code>/video')


UNKNOWN = "Unknown"
persons_face_ids = []
persons_list_result = []
video_stream = None


@bp.route('/')
def index():
    videos = Videos.query().all()
    return render_template('video/index.html', title=_('Videos Archive'), videos=videos)

# Get Video from Folder DataSet
@bp.route('/video/<filename>')
def send_video(filename):
    return send_from_directory("uploads", filename)

# Upload new video
@bp.route('/upload', methods=('GET', 'POST'))
def upload():
    form = UploadVideoForm()
    ## POST REQUEST ##
    if form.is_submitted():
        # validate that submitted data is valid
        if not form.validate():
            flash(f'Please make sure you entered all required data correctly !', 'danger')
            return redirect(request.url)
        # generate data for video
        video_file = request.files[form.video_file.name]  # get the image file
        extension = path.splitext(video_file.filename)[1]
        file_id = str(uuid1())
        file_name = file_id + extension
        # save video file to uploads folder
        video_file.save(path.join(current_app.config['UPLOADS'], file_name))
        # sava video data to the database
        new_video = Videos(name=form.name.data, file_id=file_id,
                           video_file=file_name, extension=extension)
        db.session.add(new_video)
        db.session.commit()
        flash(f'{form.name.data} was added successfully !', 'success')
        return redirect(url_for('.index'))
    ## GET REQUEST ##
    return render_template('video/upload.html', title=_('Upload Video'), form=form)


@bp.route('/edit/', methods=('GET', 'POST'))
@bp.route('/edit/<video_id>', methods=('GET', 'POST'))
def edit(video_id):
    form = EditVideoForm()
    # get camera from database
    video = Videos.query().filter(Videos.id == video_id).first()
    ## POST REQUEST ##
    if form.is_submitted():
        # validate that submitted data is valid
        if not form.validate():
            flash(f'Please make sure you entered all required data correctly !', 'danger')
            return redirect(request.url)
        video.name = form.name.data
        db.session.commit()  # save changes to database
        flash(f'{form.name.data} was updated successfully !', 'success')
        return redirect(url_for('.index', video_id=video_id))
    ## GET REQUEST ##
    # Set the form data
    form.name.data = video.name
    return render_template('video/edit.html', title=_('Edit Video'), video_id=video_id, video_file=video.video_file, form=form)


@bp.route('/delete/')
@bp.route('/delete/<video_id>')
def delete(video_id):
    try:
        video = Videos.query().filter(Videos.id == video_id).first()
        # Remove files from file system
        video_file = os.path.join(
            current_app.config["UPLOADS"], video.video_file)
        if os.path.exists(video_file):
            os.remove(video_file)
        db.session.delete(video)
        db.session.commit()  # save changes to database
        flash(f'Video was deleted successfully', 'success')
    except:
        flash(f'Failed to delete video, try again later.', 'danger')
    return redirect(url_for('.index'))

# Process video
@bp.route('/process/')
@bp.route('/process/<video_id>')
def process(video_id):
    video = Videos.query().filter(Videos.id == video_id).first()
    return render_template('video/process.html', title=video.name, video=video)


@bp.route('/videofeed/')
@bp.route('/videofeed/<video_id>')
def videofeed(video_id):
    global video_stream
    # create detection objects
    person = PersonDetection()
    face = FaceDetection()
    # get video details
    video = Videos.query().filter(Videos.id == video_id).first()
    file_path = path.join(current_app.config['UPLOADS'], video.video_file)
    video_stream = VideoCamera(file_path)
    video_stream.start()
    return Response(generatefeed(face, person, False), mimetype='multipart/x-mixed-replace; boundary=frame')

#### AJAX REQUESTS ####
@bp.route('/result')
def result():
    return jsonify(result=persons_list_result)


@bp.route('/release_resources')
def release_resources():
    global video_stream
    video_stream.stop()
    video_stream.video.release()
    print('[INFO] STREAM IS RELEASED')
    return jsonify(result=True)
######################


def generatefeed(face, person, isStream):
    global video_stream
    while True:
        # Start processes
        grabbed, frame = video_stream.get_frame()
        # If no frames were grabbed then break loop
        if not grabbed:
            break
        # Get FPS
        elapsed, fps = video_stream.get_fps()
        print(f'FRAMES PER SEC: {fps}')
        # detect faces in frame
        face_frame, face_ids = face.detect_faces(frame, isStream)
        person_frame, total, objects = person.detect_people(face_frame)
        print(f'TOTAL: {total}')
        set_persons_list(face_ids, objects)
        frame_bytes = video_stream.convert_frame_to_jpgbytes(person_frame)
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame_bytes + b'\r\n\r\n')


def set_persons_list(face_ids, objects):
    unknown_person = {'id': -1, 'file_id': '',
                      'image': 'unknown.png', 'name': UNKNOWN, 'status': ''}
    # If there were no faces detected then clear the face ids, and persons result list
    if not face_ids:
        persons_face_ids.clear()
        persons_list_result.clear()
    # Remove unknown person if no longer exists
    if UNKNOWN in persons_face_ids and not UNKNOWN in face_ids:
        persons_face_ids.remove(UNKNOWN)
        persons_list_result.remove(unknown_person)
    # Remove blacklisted person if no longer exists
    blacklist_result = [
        x for x in persons_list_result if x['status'] == 'blacklist']
    for blacklisted_person in blacklist_result:
        if not blacklisted_person['file_id'] in face_ids:
            persons_face_ids.remove(blacklisted_person['file_id'])
            persons_list_result.remove(blacklisted_person)
    # If there are faces detected then loop on thier ids
    for file_id in face_ids:
        # Check if the person's id was already added to the list, to avoid doing again a query
        if not file_id in persons_face_ids:
            persons_face_ids.append(file_id)
            face_matching = Faces.query().filter(Faces.file_id == file_id).first()
            if face_matching:
                person_view = {
                    'id': face_matching.person.id,
                    'file_id': face_matching.file_id,
                    'image': face_matching.face_image_file,
                    'name': face_matching.person.name,
                    'status': face_matching.person.status
                }
                persons_list_result.append(person_view)
            elif file_id == UNKNOWN:
                persons_list_result.append(unknown_person)
    # print(f'[INFO] RESULT LIST: {persons_list_result}')
