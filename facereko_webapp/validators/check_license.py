from flask import render_template
from datetime import datetime

EXPIRE_YEAR = 2025
EXPIRE_MONTH = 4
EXPIRE_DAY = 1

def check_expired(template: str, **kwargs):
    """
    Used to check if the license set is valid or expired the set date
    """
    # Check if exceeded time
    now = datetime.now()
    expiry_date = datetime(EXPIRE_YEAR,EXPIRE_MONTH,EXPIRE_DAY)
    # Check if demo expired
    if now > expiry_date:
        print("License expired. Please contact ITPS team for renewal.")
        return render_template('home/expired.html')
    print('License valid.')
    return render_template(template, **kwargs) 