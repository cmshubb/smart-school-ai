from werkzeug.utils import secure_filename
import functools

from flask import (Flask,
                   Blueprint, flash, g, redirect, render_template, Response, request, jsonify, session, url_for,
                   send_from_directory, current_app, abort
                   )
from werkzeug.security import check_password_hash, generate_password_hash
import face_recognition
from .forms.cars import AddCarForm, EditCarForm, SearchByPlateImage
from . import dataset
from . import db
from .models import Cars
from datetime import datetime, date
from uuid import uuid1
import os
from flask_babel import _
# from .libraries.license import PreprocessService, LPReader

bp = Blueprint('cars', __name__, url_prefix='/<lang_code>/cars')


# @bp.route('/', methods=('GET', 'POST'))
# def index():
#     # all persons
#     items = []
#     form = SearchByPlateImage()
#     found_id = None
#     ### POST REQUEST ###
#     if form.is_submitted():
#         print('SEARCHING BY LICENSE PLATE...')
#         image = request.files['image']
#         # Store image in temp
#         file_name, file_path = PreprocessService.store_image(image)
#         # Preprocess image
#         PreprocessService.preprocess_image(file_name, file_path)
#         # Extract ID from image
#         national_id_reader.front_read(file_path, True)
#         found_id = national_id_reader.IDNumber
#         # Filter persons by the ID of the image
#         print('ID NUMBER EXTRACTED IS: ', national_id_reader.IDNumber)
#         cars = Cars.query().filter(Cars.id == national_id_reader.IDNumber).all()
#     else:
#         cars = Cars.query().all()
#     for car in cars:
#         # Get first face for the person
#         car_view = create_car_view(car)
#         items.append(car_view)
#     total = len(persons)
#     blacklisted = len(Cars.query().filter(
#         Persons.status == 'blacklist').all())
#     whitelisted = len(Cars.query().filter(
#         Persons.status == 'whitelist').all())
#     unknown = len(Cars.query().filter(Cars.name == 'Unknown').all())
#     counters = {'total_count': total, 'blacklist_count': blacklisted,
#                 'whitelist_count': whitelisted, 'unknown_count': unknown}
#     # Return List Of Images From DataSet
#     # relevant_path = "./facereko_webapp/dataset"
#     # included_extensions = ['jpg', 'JPG', 'jpeg', 'bmp', 'png', 'gif']
#     # images = [fn for fn in os.listdir(relevant_path)
#     #          if any(fn.endswith(ext) for ext in included_extensions)]
#     # images = os.listdir('./facereko_webapp/dataset')
#     return render_template('cars/index.html', counters=counters, cars=items, form=form, found_id=found_id)


# # Get Image from Folder DataSet

# @bp.route('/image/<filename>')
# def send_image(filename):
#     return send_from_directory("dataset", filename)


# # Add New Person ON Dataset Folder
# @bp.route('/add', methods=('GET', 'POST'))
# def add():
#     form = AddCarForm()
#     ### POST REQUEST ###
#     if form.is_submitted():
#         # validate that submitted data is valid
#         if not form.validate():
#             flash(f'Please make sure you entered all required data correctly !', 'danger')
#             return redirect(request.url)
#         # validate that there is and uploaded image
#         if not form.plate_image.data:
#             flash(f'Please upload an image for the car !', 'danger')
#             return redirect(request.url)
#         # check if uploaded file is an allowed image file
#         if not allowed_image(image_file.filename):
#             flash(f'Please upload a valid image file !', 'danger')
#             return redirect(request.url)

#         # add person to the database
#         new_car = Cars(name=form.name.data, plate_image_file=form.plate_filename.data, status=form.status.data, color=form.color.data, owner=form.owner.data)
#         db.session.add(new_car)
#         db.session.commit()  # save changes to database
#         flash(f'{form.name.data} was added successfully !', 'success')
#         return redirect(url_for('.index'))

#     ## GET REQUEST ##
#     return render_template('/cars/add.html', title=_('Add Car'), form=form)


# @bp.route('/car/')
# @bp.route('/car/<car_id>')
# def person(car_id):
#     # get person from database
#     car = Cars.query().filter(Cars.id == car_id).first()
#     car_view = create_car_view(car)
#     return render_template('cars/car.html', title=car.name, car=car_view)


# @bp.route('/edit/', methods=('GET', 'POST'))
# @bp.route('/edit/<car_id>', methods=('GET', 'POST'))
# def edit(car_id):
#     form = EditCarForm()
#     # get person from database
#     car = Cars.query().filter(Cars.id == car_id).first()
#     ### POST REQUEST ###
#     if form.is_submitted():
#         # validate that submitted data is valid
#         if not form.validate():
#             flash(f'Please make sure you entered all required data correctly !', 'danger')
#             return redirect(request.url)
#         car.name = form.name.data
#         car.status = form.status.data
#         car.color = form.color.data
#         car.owner = form.owner.data
#         db.session.commit()  # save changes to database
#         flash(f'{form.name.data} was updated successfully !', 'success')
#         return redirect(url_for('.car', car_id=car_id))
#     ## GET REQUEST ##
#     # Set the form data
#     form.name.data = car.name
#     form.status.data = car.status
#     form.color.data = car.color
#     form.owner.data = car.owner
#     return render_template('cars/edit.html',  title=_('Edit Car'), form=form, car_id=car.id, image=car.plate_image_file)

# @bp.route('/delete/')
# @bp.route('/delete/<car_id>')
# def delete(car_id):
#     try:
#         car = Cars.query().filter(Cars.id == car_id).first()
#         # Remove files from file system
#         image_file = os.path.join(current_app.config["DATASET"], car.plate_image_file)
#         if os.path.exists(image_file):
#             os.remove(image_file)
#         db.session.delete(car)
#         db.session.commit()  # save changes to database
#         flash(f'Car was deleted successfully', 'success')
#     except:
#         flash(f'Failed to delete car, try again later.', 'danger')
#     return redirect(url_for('.index'))

# ####### HELPER FUNCTIONS ########


# def create_car_view(car: Cars):
#     # Create person view to display in template
#     car_view = {
#         'id': car.id,
#         'name': car.name,
#         'status': car.status,
#         'plate_number': car.plate_numbers,
#         'plate_chars': car.plate_characters,
#         'image': car.plate_image_file,
#     }
#     return car_view


# def allowed_image(filename):
#     # Check Image Extention "JPEG", "JPG", "PNG", "GIF"
#     if not "." in filename:
#         return False

#     ext = filename.rsplit(".", 1)[1]

#     if ext.upper() in current_app.config["ALLOWED_IMAGE_EXTENSIONS"]:
#         return True
#     else:
#         return False


def calculate_age(born):
    # calculate age from datetime
    if born:
        today = date.today()
        return today.year - born.year - ((today.month, today.day) < (born.month, born.day))
    return None
####### HELPER FUNCTIONS ########
