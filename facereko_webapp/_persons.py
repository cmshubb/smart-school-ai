from werkzeug.utils import secure_filename
import functools

from flask import (Flask,
                   Blueprint, flash, g, redirect, render_template, Response, request, jsonify, session, url_for,
                   send_from_directory, current_app, abort
                   )
from werkzeug.security import check_password_hash, generate_password_hash
import face_recognition
from .forms.persons import AddPersonForm, EditPersonForm, AddFaceForm, SearchByCivilIdImage
from . import dataset
from . import db
from .models import Persons, Faces
from datetime import datetime, date
from uuid import uuid1
import os
from flask_babel import _
from .libraries.ocr.card_processing_service import CardProcessingService
from .libraries.ocr import national_id_reader
import sqlalchemy

bp = Blueprint('persons', __name__, url_prefix='/<lang_code>/persons')


@bp.route('/', methods=('GET', 'POST'))
def index():
    # all persons
    items = []
    form = SearchByCivilIdImage()
    found_id = None
    ### POST REQUEST ###
    if form.is_submitted():
        processing_service = CardProcessingService()
        print('SEARCHING BY CIVIL ID...')
        image = request.files['image']
        # Store image in temp
        front_file_name, front_file_path = processing_service.store_image('_front', image)
        # Preprocess image
        front_processed_file_name, front_processed_file_path = processing_service.preprocess_image(front_file_name, front_file_path)
        # Extract ID from image
        national_id_reader.front_read(front_processed_file_path, True)
        found_id = national_id_reader.IDNumber
        # Filter persons by the ID of the image
        print('ID NUMBER EXTRACTED IS: ', national_id_reader.IDNumber)
        persons = Persons.query().filter(Persons.civil_id == national_id_reader.IDNumber).all()
    else:
        persons = Persons.query().all()
    for person in persons:
        # Get first face for the person
        person_view = create_person_view(person)
        items.append(person_view)
    total = len(persons)
    blacklisted = len(Persons.query().filter(
        Persons.status == 'blacklist').all())
    whitelisted = len(Persons.query().filter(
        Persons.status == 'whitelist').all())
    unknown = len(Persons.query().filter(Persons.name == 'Unknown').all())
    counters = {'total_count': total, 'blacklist_count': blacklisted,
                'whitelist_count': whitelisted, 'unknown_count': unknown}
    # Return List Of Images From DataSet
    # relevant_path = "./facereko_webapp/dataset"
    # included_extensions = ['jpg', 'JPG', 'jpeg', 'bmp', 'png', 'gif']
    # images = [fn for fn in os.listdir(relevant_path)
    #          if any(fn.endswith(ext) for ext in included_extensions)]
    # images = os.listdir('./facereko_webapp/dataset')
    return render_template('persons/index.html', counters=counters, persons=items, form=form, found_id=found_id)


# Get Image from Folder DataSet

@bp.route('/image/<filename>')
def send_image(filename):
    return send_from_directory("dataset", filename)


# Add New Person ON Dataset Folder
@bp.route('/add', methods=('GET', 'POST'))
def add():
    form = AddPersonForm()
    ### POST REQUEST ###
    if form.is_submitted():
        # validate that submitted data is valid
        if not form.validate():
            flash(f'Please make sure you entered all required data correctly !', 'danger')
            return redirect(request.url)
        # validate that there is and uploaded image
        if not form.face_image.data:
            flash(f'Please upload an image for the person !', 'danger')
            return redirect(request.url)
        # generate unique file id and save it to uploads
        image_file = request.files[form.face_image.name]  # get the image file
        national_id_front = request.files[form.national_id_front_image.name] # get national id front
        # prepare image for processing
        extension = os.path.splitext(image_file.filename)[1]
        file_id = str(uuid1())
        file_name = file_id + extension
        national_front_file_name = file_id + '_front' + extension
        encoded_face_file = file_id + '.json'
        # check if uploaded file is an allowed image file
        if not allowed_image(image_file.filename):
            flash(f'Please upload a valid image file !', 'danger')
            return redirect(request.url)
        # save image to uploads directory
        image_file.save(os.path.join(current_app.config["DATASET"], file_name))
        national_id_front.save(os.path.join(current_app.config["DATASET"], national_front_file_name))
        encode_face_result = dataset.encode_file(
            current_app.config["DATASET"], file_name, file_id)
        if not encode_face_result:
            flash(f'Uploaded image has no valid faces, please try again !', 'danger')
            return redirect(request.url)

        # add person to the database
        new_person = Persons(name=form.name.data, civil_id_image_file=form.national_id_filename.data, status=form.status.data, civil_id=form.civil_id.data,
                             date_of_birth=form.date_of_birth.data, gender=form.gender.data)
        new_person.faces.append(Faces(
            face_image_file=file_name, face_encoding_file=encoded_face_file, file_id=file_id))
        db.session.add(new_person)
        db.session.commit()  # save changes to database
        flash(f'{form.name.data} was added successfully !', 'success')
        return redirect(url_for('.index'))

    ## GET REQUEST ##
    return render_template('/persons/add.html', title=_('Add Person'), form=form)


@bp.route('/person/')
@bp.route('/person/<person_id>')
def person(person_id):
    # get person from database
    person = Persons.query().filter(Persons.id == person_id).first()
    person_view = create_person_view(person)
    return render_template('persons/person.html', title=person.name, person=person_view)
    # return redirect(url_for('persons/person.html', title=person.name, person=person_view))


@bp.route('/edit/', methods=('GET', 'POST'))
@bp.route('/edit/<person_id>', methods=('GET', 'POST'))
def edit(person_id):
    form = EditPersonForm()
    # get person from database
    person = Persons.query().filter(Persons.id == person_id).first()
    ### POST REQUEST ###
    if form.is_submitted():
        # validate that submitted data is valid
        if not form.validate():
            flash(f'Please make sure you entered all required data correctly !', 'danger')
            return redirect(request.url)
        person.name = form.name.data
        person.status = form.status.data
        person.civil_id = form.civil_id.data
        person.gender = form.gender.data
        person.date_of_birth = form.date_of_birth.data
        db.session.commit()  # save changes to database
        flash(f'{form.name.data} was updated successfully !', 'success')
        return redirect(url_for('.person', person_id=person_id))
    ## GET REQUEST ##
    # Set the form data
    form.name.data = person.name
    form.status.data = person.status
    form.civil_id.data = person.civil_id
    form.gender.data = person.gender
    form.date_of_birth.data = person.date_of_birth
    return render_template('persons/edit.html',  title=_('Edit Person'), form=form, person_id=person.id, image=person.faces[0].face_image_file)


@bp.route('/add_face/', methods=('GET', 'POST'))
@bp.route('/add_face/<person_id>', methods=('GET', 'POST'))
def add_face(person_id):
    form = AddFaceForm()
    # get person from database
    person = Persons.query().filter(Persons.id == person_id).first()
    ### POST REQUEST ###
    if form.is_submitted():
        # validate that submitted data is valid
        if not form.validate():
            flash(f'Please make sure you entered all required data correctly !', 'danger')
            return redirect(request.url)
        # generate unique file id and save it to uploads
        image_file = request.files[form.face_image.name]  # get the image file
        extension = os.path.splitext(image_file.filename)[1]
        file_id = str(uuid1())
        file_name = file_id + extension
        encoded_face_file = file_id + '.json'
        # check if uploaded file is an allowed image file
        if not allowed_image(image_file.filename):
            flash(f'Please upload a valid image file !', 'danger')
            return redirect(request.url)
        # save image to uploads directory
        image_file.save(os.path.join(current_app.config["DATASET"], file_name))
        encode_face_result = dataset.encode_file(
            current_app.config["DATASET"], file_name, file_id)
        if not encode_face_result:
            flash(f'Uploaded image has no valid faces, please try again !', 'danger')
            return redirect(request.url)
        # commit face to database
        person.faces.append(Faces(face_image_file=file_name,
                                  face_encoding_file=encoded_face_file, file_id=file_id))
        db.session.commit()  # save changes to database
        flash(f'{person.name} face was added successfully !', 'success')
        return redirect(url_for('.person', person_id=person_id))
    ## GET REQUEST ##
    person_view = create_person_view(person)
    return render_template('persons/add_face.html', title=person.name, person=person_view, form=form)


@bp.route('/delete/')
@bp.route('/delete/<person_id>')
def delete(person_id):
    try:
        person = Persons.query().filter(Persons.id == person_id).first()
        # Remove faces for the person
        for face in person.faces:
            db.session.delete(face)
            # Remove files from file system
            image_file = os.path.join(
                current_app.config["DATASET"], face.face_image_file)
            encoded_file = os.path.join(
                current_app.config["DATASET"], face.face_encoding_file)
            if os.path.exists(image_file):
                os.remove(image_file)
            if os.path.exists(encoded_file):
                os.remove(encoded_file)
        db.session.delete(person)
        db.session.commit()  # save changes to database
        flash(f'Person was deleted successfully', 'success')
    except:
        flash(f'Failed to delete person, try again later.', 'danger')
    return redirect(url_for('.index'))

####### HELPER FUNCTIONS ########


def create_person_view(person):
    # Create person view to display in template
    face = person.faces[0]
    date_of_birth = None
    if person.date_of_birth:
        date_of_birth = person.date_of_birth.strftime('%d/%m/%Y')
    person_view = {
        'id': person.id,
        'name': person.name,
        'status': person.status,
        'civil_id': person.civil_id,
        'civil_id_image': person.civil_id_image_file,
        'date_of_birth': date_of_birth,
        'age': calculate_age(person.date_of_birth),
        'gender': person.gender,
        'image': face.face_image_file,
        'faces': person.faces,
        'faces_count': len(person.faces)
    }
    return person_view


def allowed_image(filename):
    # Check Image Extention "JPEG", "JPG", "PNG", "GIF"
    if not "." in filename:
        return False

    ext = filename.rsplit(".", 1)[1]

    if ext.upper() in current_app.config["ALLOWED_IMAGE_EXTENSIONS"]:
        return True
    else:
        return False


def calculate_age(born):
    # calculate age from datetime
    if born:
        today = date.today()
        return today.year - born.year - ((today.month, today.day) < (born.month, born.day))
    return None
####### HELPER FUNCTIONS ########
