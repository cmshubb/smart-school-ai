"""
Some of this code is the same or an edited version from Adrian Rosebrock's "YOLO object detection with OpenCV" article
on his PyImageSearch Blog.
You can find it here: https://www.pyimagesearch.com/2018/11/12/yolo-object-detection-with-opencv/
"""
import cv2
import os
import numpy as np
from flask import (Flask,
                   Blueprint, flash, g, redirect, render_template, Response, request, jsonify, session, url_for,
                   send_from_directory, current_app
                   )
from PIL import ImageFont, ImageDraw, Image
from .OCRandPreprocess import OCR_Letters, OCR_Numbers, OCR_Plate_NotCropped
from .CropAndResize import crop
import tensorflow as tf  # Import is here so we don't waste time if there's no model
MODEL = tf.keras.models.load_model(os.path.join(current_app.config['YOLO_MODEL'], "model.h5"))


confThreshold = 0.5
nmsThreshold = 0.5

inpWidth = 416  # Width of YoloTiny network's input image
inpHeight = 416  # Height of YoloTiny network's input image

# Load the configuration
modelConfiguration = os.path.join(
    current_app.config['YOLO_MODEL'], "yolov3-tiny.cfg")
modelWeights = os.path.join(
    current_app.config['YOLO_MODEL'], "yolov3-tiny.backup")
net = cv2.dnn.readNetFromDarknet(modelConfiguration, modelWeights)
net.setPreferableBackend(cv2.dnn.DNN_BACKEND_OPENCV)
net.setPreferableTarget(cv2.dnn.DNN_TARGET_CPU)

outputFile = "out.avi"

YoloClasses = 'LP'
alphabet = {
    "a": "أ", "b": "ب", "t": "ت", "th": "ث", "g": "ج", "hh": "ح", "kh": "خ", "d": "د", "the": "ذ",
    "r": "ر", "z": "ز", "c": "س", "sh": "ش", "s": "ص", "dd": "ض", "tt": "ط", "zz": "ظ", "i": "ع",
    "gh": "غ", "f": "ف", "q": "ق", "k": "ك", "l": "ل", "m": "م", "n": "ن", "h": "ه", "w": "و",
    "y": "ي", "0": "٠", "1": "١", "2": "٢", "3": "٣", "4": "٤", "5": "٥", "6": "٦", "7": "٧",
    "8": "٨", "9": "٩"
}

classes = list(alphabet.keys())


# Get the names of the output layers
def getOutputsNames(n):
    # Get the names of all the layers in the network
    layersNames = n.getLayerNames()
    # Get the names of the output layers, i.e. the layers with unconnected outputs
    return [layersNames[i[0] - 1] for i in n.getUnconnectedOutLayers()]


# Draw the predicted bounding box
def drawPred(fr, classId, conf, left, top, right, bottom):
    # Draw a bounding box.
    cv2.rectangle(fr, (left, top), (right, bottom), (255, 255, 255), 3)
    # Get the label for the class name and its confidence
    lab = '%s:%.2f' % (YoloClasses[classId], conf)
    # Display the label at the top of the bounding box
    labelSize, baseLine = cv2.getTextSize(
        lab, cv2.FONT_HERSHEY_SIMPLEX, 0.5, 1)
    top = max(top, labelSize[1])
    cv2.rectangle(fr, (left, top - round(1.5 * labelSize[1])), (left + round(1.5 * labelSize[0]), top + baseLine),
                  (0, 0, 255), cv2.FILLED)
    cv2.putText(fr, lab, (left, top),
                cv2.FONT_HERSHEY_SIMPLEX, 0.75, (0, 0, 0), 2)


# Remove the bounding boxes with low confidence using non-maxima suppression
def postprocess(fr, outs, confT, nmsT):
    frameHeight = fr.shape[0]
    frameWidth = fr.shape[1]

    # Scan through all the bounding boxes output from the network and keep only the
    # ones with high confidence scores. Assign the box's class label as the class with the highest score.
    classIds = []
    confidences = []
    boxes = []

    for o in outs:
        for detection in o:
            scores = detection[5:]
            classId = np.argmax(scores)
            confidence = scores[classId]
            if confidence > confT:
                center_x = int(detection[0] * frameWidth)
                center_y = int(detection[1] * frameHeight)
                width = int(detection[2] * frameWidth)
                height = int(detection[3] * frameHeight)
                left = int(center_x - width / 2)
                top = int(center_y - height / 2)
                classIds.append(classId)
                confidences.append(float(confidence))
                boxes.append([left, top, width, height])

    # Perform non maximum suppression to eliminate redundant overlapping boxes with lower confidences.
    indices = cv2.dnn.NMSBoxes(boxes, confidences, confT, nmsT,)
    cropped = None
    for i in indices:
        i = i[0]
        box = boxes[i]
        left = max(box[0], 0)
        top = max(box[1], 0)
        width = max(box[2], 0)
        height = max(box[3], 0)
        if height > width:
            continue
        cropped = fr[top:(top + height), left:(left + width)]
        amr = 0
        cv2.imwrite(os.path.join(
            'Outputs', 'ITPS{:>05}.jpg'.format(amr)), cropped)
        amr += 1
        print("[INFO] Saved License Successfully")
        drawPred(fr, classIds[i], confidences[i],
                 left, top, left + width, top + height)

    return len(indices) > 0, cropped


def read():
    while True:
        # output folder of yolo Detections
        img_dir = current_app.config['PLATES']

        # Enter Directory of all images
        data_path = os.path.join(img_dir, '*g')
        files = glob.glob(data_path)
        data = []

        # if the directory is empty wait 20 sec
        if os.listdir(img_dir) == []:
            # print("No images found")
            time.sleep(3)
        else:
            # print("image is found")
            # looping inside the ouptuts folder for images
            for f1 in files:

                # reading the image
                img = cv2.imread(f1)

                # appending the images
                data.append(img)
                try:
                    # after we succssfuly finding the image in the folder and detecting license plate
                    # we remove the selected image from the ouptuts folder
                    # apply CropAndResize &OCRandPreprocess modules to read off the arabic characters off the image

                    # CropAndResize.crop(f1)
                    OCRandPreprocess.OCR_Letters()
                    OCRandPreprocess.OCR_Numbers()
                    OCRandPreprocess.OCR_Plate_NotCropped()
                    os.remove(f1)

                # if the image cannot be shown print error
                except cv2.error as e:
                    print('Cannot  find the image ')
                    os.remove(f1)


def detect_plate(file_path):
    image = cv2.imread(file_path)
    frHeight, frWidth, channels = img.shape
    # frWidth = round(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    # frHeight = round(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

    # Create a 4D blob from an image.
    blob = cv2.dnn.blobFromImage(image, 1 / 255, (inpWidth, inpHeight), [0, 0, 0], 1, crop=False)
    # Sets the input to the network
    net.setInput(blob)
    # Runs the forward pass to get output of the output layers
    run = net.forward(getOutputsNames(net))
    # Remove the bounding boxes with low confidence
    rec, plateImg = postprocess(image, run, confThreshold, nmsThreshold)
    return rec, plateImg
    #cv2.imshow("Capture", cv2.resize(image, (400, 300)))
    # If there's still a plate, recognize the characters
    # if rec and plateImg is not None:
    #     read()

    #     # Write the frame with the detection boxes
    #     if args.image:
    #         cv2.imwrite(outputFile, image.astype(np.uint8))
    #     else:
    #         vid_writer.write(frame.astype(np.uint8))
