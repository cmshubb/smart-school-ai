import numpy as np
import cv2
import imutils
import sys
import pytesseract
import time
import arabic_reshaper   
import bidi.algorithm 
from flask import Flask
import os
#setting an output folder
Output= os.path.join('static', 'Output')
app = Flask(__name__)
app.config['Output'] = Output


pytesseract.pytesseract.tesseract_cmd = r"C:/Program Files/Tesseract-OCR/tesseract.exe"

def preprocess(img):
    # print ('preprocessing image')
    # cv2.imshow("Input", img)
    # Resize the image - change width to 500
   
    # RGB to Gray scale conversion
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # cv2.imshow("1 - Grayscale Conversion", gray)#1
    # cv2.waitKey(0)
    fastNlMeansDenoising=cv2.fastNlMeansDenoising(gray,None,10,7,21)
    # cv2.imshow("fast Means Denoising", fastNlMeansDenoising)
    # cv2.waitKey()

    medianBlur = cv2.medianBlur(fastNlMeansDenoising, 3)
    # cv2.imshow("medianBlur", medianBlur)   
    # cv2.waitKey(0)
    bilateralFilter = cv2.bilateralFilter(medianBlur, 11, 17, 17)
    # cv2.imshow("2 - Bilateral Filter", gray)#2
    # cv2.waitKey(0)
    thresholding = cv2.threshold(bilateralFilter, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
    # cv2.imshow("thresholding", thresholding)
    # cv2.waitKey()

    
    return thresholding
#this function is made to mirror output text
def reverse_text(reverse):
   return reverse[::-1]

def OCR_Letters():
# Read the image file
    img = cv2.imread(os.path.join(app.config['Output'],"letters.jpg"))
    #applying threshold
    thresholding = preprocess(img)
    #converting image to text by tesseract 
    config =('--oem 3 -l ITPS_ARABIC --psm 6 ')
    text = pytesseract.image_to_string(thresholding, config=config,)

    sys.stdout=open(os.path.join(app.config['Output'],"test.txt"),"w",encoding="utf-8")
    print(text)
    sys.stdout.close()

    cv2.waitKey(10)
    cv2.destroyAllWindows()

def OCR_Numbers():
# Read the image file
    img = cv2.imread(os.path.join(app.config['Output'],"numbers.jpg"))
    #applying threshold
    thresholding = preprocess(img)
    #converting image to text by tesseract 
    config =('--oem 3 -l ITPS_ARABIC_ADDRESS  --psm 6 ') 
    number = pytesseract.image_to_string(thresholding, config=config,)
    sys.stdout=open(os.path.join(app.config['Output'],"test.txt"),"a",encoding="utf-8")
    print(reverse_text(number))
    sys.stdout.close()
    cv2.waitKey(10)
    cv2.destroyAllWindows()


# def OCR_Plate():
# # Read the image file
#     img = cv2.imread(os.path.join(app.config['Output'],"plate.jpg"))
#     #applying threshold
#     thresholding = preprocess(img)
#     #converting image to text by tesseract 
#     config =('--oem 3 -l ITPS_ARABIC_ADDRESS  --psm 6 ') 
#     number = pytesseract.image_to_string(thresholding, config=config,)
#     sys.stdout=open(os.path.join(app.config['Output'],"test.txt"),"a",encoding="utf-8")
#     print(reverse_text(number))
#     sys.stdout.close()
#     cv2.waitKey(10)
#     cv2.destroyAllWindows()


def OCR_Plate_NotCropped():
# Read the image file
    img = cv2.imread(os.path.join(app.config['Output'],"Not Cropped plate.jpg"))

    #applying threshold
    thresholding = preprocess(img)

    scale_percent = 60 # percent of original size
    width = int(thresholding.shape[1] * scale_percent / 100)
    height = int(thresholding.shape[0] * scale_percent / 100)
    dim = (width, height)

    # resize image
    resized = cv2.resize(thresholding, dim, interpolation = cv2.INTER_AREA)

    #converting image to text by tesseract 
    config =('--oem 3 -l ITPS_ARABIC_ADDRESS  --psm 6 ') 

    PlateNotCropped = pytesseract.image_to_string(resized, config=config,)
    sys.stdout=open(os.path.join(app.config['Output'],"PlateNotCropped.txt"),"w",encoding="utf-8")
   
    print(PlateNotCropped)
    
    sys.stdout.close()
    cv2.waitKey(10)
    cv2.destroyAllWindows()

# Noise removal with iterative bilateral filter(removes noise while preserving edges)
# gray2 = cv2.bilateralFilter(gray, 11, 17, 17)
# cv2.imshow("2 - Bilateral Filter", gray)#2
# cv2.waitKey(0)

# edged = cv2.Canny(gray, 170, 200)
# cv2.imshow("3 - Canny Edges", edged)#3
# cv2.waitKey(0)