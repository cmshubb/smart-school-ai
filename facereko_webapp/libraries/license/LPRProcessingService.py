# import the necessary packages
import cv2
import os
from PIL import Image
import numpy as np
from flask import (Flask,
                   Blueprint, flash, g, redirect, render_template, Response, request, jsonify, session, url_for,
                   send_from_directory, current_app
                   )
from uuid import uuid4

#setting an output folder

def store_image(file):
    file_id = str(uuid4())
    extension = os.path.splitext(file.name)[1]
    print(file.name)
    file_name = file_id + side + extension
    temp_file_location = os.path.join(current_app.config['TEMP'], file_name)
    file.save(temp_file_location)
    return temp_file_location, file_id

def preprocess_image(file_path, file_id):
# load the image and show it
    image = cv2.imread(file_path)
# we need to keep in mind aspect ratio so the image does
# not look skewed or distorted -- therefore, we calculate
# the ratio of the new image to the old image
#image size (400, 400 ,3) (height, width, RGB)
    r = 400 / image.shape[1]
    dim = (400, int(image.shape[0] * r))
# perform the actual resizing of the image and show it
    resized = cv2.resize(image, dim, interpolation = cv2.INTER_AREA)
    
    filename = os.path.join(app.config['PLATES'], f"{file_id}_resized.png")
    # cv2.imwrite(filename, resized) 
    # resized = resized.save("test-600.png", dpi=(600,600))

    
    # resized = resized.save("test-600.png", dpi=(600,600))
    # resized=resized.save("test-600.png", dpi=(600,600))

    # print(resized.shape)
# crop the image
#crop the image to the side of letters
#[Top:bottom, Left:Right]
    # letters = resized[90:180, 210:400]
    letters = resized[80:200, 210:400]
    filename = os.path.join(app.config['PLATES'], f"{file_id}_letters.jpg")
    cv2.imwrite(filename, letters) 
#crop the image to the side of numbers
    # numbers = resized[80:180, 15:190]
    # numbers = resized[10:180, 8:180]
    numbers = resized[80:180, 15:190]
    filename = os.path.join(app.config['PLATES'], f"{file_id}_numbers.jpg")
    cv2.imwrite(filename, numbers) 

#crop the image to the plate (both Letters&Numbers) 
    # Plate = resized[90:180, 1:400]
    Plate = resized[1:180, 1:400]
    filename = os.path.join(app.config['PLATES'], f"{file_id}_plate.jpg")
    cv2.imwrite(filename, Plate) 

    PlateNotCropped = image
    filename = os.path.join(app.config['PLATES'], f"{file_id}_Non_Cropped_Plate.jpg")
    cv2.imwrite(filename, PlateNotCropped) 