import cv2
import PIL
import numpy as np
import base64

ALL_CONTOURS = -1 # in order to pass All Contours pass -1 in drawContours
CONTOUR_COLOR = (0, 0, 255) # Red COLOR 
THICKNESS = 3 # Thickness of the contour on image
class MotionDetectionModel:



    
    def detect_motion(self, image1, image2):
        """
        Detect motion in an image if they exist.
        Return a list of Contours tuple of each detected motion

        """


        difference = cv2.absdiff(image1, image2)
        # converting the original Frame into Gray
        gray = cv2.cvtColor(difference,cv2.COLOR_BGR2GRAY)
        # Applying Gaussian Thresholding to the Gray Frame
        # (5,5): Kernal Size , if you increase the numbers then it will be more blur
        # The zero refer to Kernel standard deviation along X-axis (horizontal direction).
        # if it is zero then the SigmaY (Vertical direction) is set to zero
        blur = cv2.GaussianBlur(gray,(5,5),0)
        # Apply Threshold to Gaussian
        ret, threshold = cv2.threshold(blur, 20, 255, cv2.THRESH_BINARY)
        # Dilated:  It is just opposite of erosion
        # Remove Noises, So it increases the white region in the image
        dilated = cv2.dilate(threshold, np.ones((3, 3), np.uint8), iterations=1)
        # erodes away the boundaries of foreground object
        eroded = cv2.erode(dilated, np.ones((3, 3), np.uint8), iterations=1)
        # Find contours in the image
        contours, h = cv2.findContours(eroded, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        return image1, contours

    def draw_motion_on_image(self, image1, image2,contours):
        """
        Draw Contours on the image 
        return draw contours
        """
        image1,contours = self.detect_motion(image1,image2)
        draw_contour = cv2.drawContours(image1,contours,ALL_CONTOURS,CONTOUR_COLOR,THICKNESS)
        return draw_contour

    def convert_image_to_jpgbytes(self, image):
        encoded, jpeg = cv2.imencode('.jpg', image)
        jpg_as_bytes = base64.b64encode(jpeg)
        return jpg_as_bytes

        