# import the necessary packages
from .transform import four_point_transform
from skimage.filters import threshold_local #this is used for local adaptive thresholding
import numpy as np
# import argparse  # this library was used for testing purposes  
import cv2
import imutils #Image Processing library (resizing,contours)
import os
from flask import (Flask,
                   Blueprint, flash, g, redirect, render_template, Response, request, jsonify, session, url_for,
                   send_from_directory, current_app
                   )
from .warp_service import WarpService
from uuid import uuid4

class CardProcessingService(object):
    def __init__(self):
        self.warp_service = WarpService()

    def store_image(self, side, file):
        """
        Generate a unique file_id for the image and store it to UPLOADS_ROOT
        """
        file_id = str(uuid4())
        extension = os.path.splitext(file.name)[1]
        print(file.name)
        file_name = file_id + side + extension
        temp_file_location = os.path.join(current_app.config['TEMP'], file_name)
        file.save(temp_file_location)
        print('[INFO] IMAGE SAVED: ', temp_file_location)
        return file_name, temp_file_location

    def preprocess_image(self, file_name, path):
        """
        Resize the original image and warp it to become aligned.
        """
        screenCnt = []  # This will hold the final contour
        # load the image and compute the ratio of the old height
        # to the new height, clone it, and resize it
        # we resize the original image to 500 pixels
        # to unify any size of any image to fixed size (500pixels)
        image = cv2.imread(path)
        ratio = image.shape[0] / 500.0
        orig = image.copy()
        image = imutils.resize(image, height=500)

        # convert the image from RGB to grayscale, blur it, and find edges
        # in the image
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        ksize = 1
        found_edges = False
        for x in range(5):
            if not x == 0:
                ksize = ksize + 2
            gray = cv2.GaussianBlur(gray, (ksize, ksize), 0)
            # edged = cv2.Canny(gray, 75, 200)
            edged = cv2.Canny(gray, 100, 200)  # edge detection

            # find the contours in the edged image, keeping only the
            # largest ones, and initialize the screen contour
            # find contours of ID
            cnts = cv2.findContours(edged.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
            cnts = imutils.grab_contours(cnts)
            # examine only the largest of the contours, discarding the rest.
            cnts = sorted(cnts, key=cv2.contourArea, reverse=True)[:5]
            
            ##### USE THIS TO TEST EDGE DETECTION #####
            # cv2.drawContours(image, cnts, -1, (0, 255, 0), 3)
            # cv2.imshow('Draw Contours', image)
            # cv2.waitKey(0)
            # cv2.destroyAllWindows()
            
            # loop over the contours
            for c in cnts:
                # approximate the contour
                peri = cv2.arcLength(c, True)
                approx = cv2.approxPolyDP(c, 0.02 * peri, True)

                # if our approximated contour has four points, then we
                # can assume that we have found our screen
                if len(approx) == 4:
                    screenCnt = approx
                    found_edges = True
                    break
            # Break if the matching edges are found
            if found_edges:
                break

        if len(screenCnt):
            # apply the four point transform to obtain a top-down
            # view of the original image
            warped = self.warp_service.four_point_transform(
                orig, screenCnt.reshape(4, 2) * ratio)
            # show the original and scanned images
            newimg = cv2.resize(warped, (1000, 630))
            processed_file_name = f"{file_name}_processed.jpg"

            file_path = os.path.join(current_app.config['DATASET'], processed_file_name)
            cv2.imwrite(file_path, newimg)
            return processed_file_name, file_path
        return None
       
