from PIL import Image  # Load Images from files
import cv2
from pytesseract import image_to_string  # tesseract engine used for ocr
import numpy as np
import os
from .pob import placeOfBirth  # define place of birth from ID number [7:9]
from .gender import gen  # define gender from ID number [12:13]
pic = np.zeros((300, 225))
name, add, IDNumber, BDate = '', '', '', ''
job, job2, gender, religion, a3zb, husband, govern = '', '', '', '', '', '', ''
# Reading the front side of the ID card

def get_full_year(year):
    year_int = int(year)
    if year_int < 99 and year_int > 22:
        year = '19' + year
    else:
        year = '20' + year
    return year

def front_read(front_src, is_search=False):

    im_gray = cv2.imread(front_src, cv2.IMREAD_GRAYSCALE)
    im_bw = im_gray
    thresh = 95
    im_bw = cv2.threshold(im_bw, thresh, 255, cv2.THRESH_BINARY)[1]
    global pic
    global name
    global add
    global IDNumber
    global BDate
    global govern
    # split the img
    # Takes on dimensions for picture on ID [horziontal,Vertical]
    pic = im_gray[50:350, 10:275]
    # Takes on dimensions for Name on ID [horziontal,Vertical]
    Name = im_gray[150:310, 400:1000]
    first_Name = im_gray[160:225, 700:1000]
    last_Name = im_gray[223:299, 400:1000]

    #(thresh, first_Name) = cv2.threshold(first_Name, 127, 255, cv2.THRESH_BINARY)
    #(thresh, last_Name) = cv2.threshold(last_Name, 127, 255, cv2.THRESH_BINARY)
    #last_Name = cv2.medianBlur(last_Name,3)

    # Name = im_gray[141:282,]
    #cv2.imshow('first_name', first_Name)
    #cv2.imshow('last_name', last_Name)
    #cv2.waitKey(0)
    #cv2.destroyAllWindows()  


    # Takes on dimensions for Address on ID [horziontal,Vertical]
    address = im_bw[300:450, 400:1000]
    # Takes on dimensions for ID number on ID [horziontal,Vertical]
    ID = im_bw[500:560, 400:1000]

    # Setting page segmentation modes
    first_config = ("--oem 1 --psm 6")     # Assume a single uniform block of text
    last_config = ("--oem 1 --psm 7")      # Treat the image as a single text line
    

    first_name = image_to_string(first_Name, lang='ara', config=first_config)
    first_name = first_name[:-1]
   

    last_name = image_to_string(last_Name, lang='ara', config=last_config)
    last_name = last_name[:-1]
    
    name = first_name + ' ' + last_name


    add = image_to_string(address, lang='ara_finetuned')
    # We used a fine tuned dataset specific for arabic numbers
    IDNumber = image_to_string(ID, lang='ara_number')
    IDNumber = ''.join(IDNumber.split())

    # Calculate the birth date
    year = '' + IDNumber[1:3]
    month = IDNumber[3:5]
    day = IDNumber[5:7]
    BDate = day + '-' + month + '-' + get_full_year(year)
    # Getting the governorate.
    govern = placeOfBirth(IDNumber)
    if is_search:
        os.remove(front_src)


# split image
# clip images according to given dimensions and extract the text from it
def crop(dim1, dim2, dim3, dim4, name, img_binary, text_all):
    area = (dim1, dim2, dim3, dim4)
    cropped_img = img_binary.crop(area)  # crop the specified area from ID
    # extract the text from specified area
    text = image_to_string(cropped_img, lang='ara')
# cropped_img.show()
    # if religion=='religion':

    # 	if 'مسلم' in text_all:
    # 		text='مسلم'
    # 	elif 'مسيحي' in text_all:
    # 		text='مسيحي'
    # 	elif 'مسيحى' in text_all:
    # 		text='مسيحي'
    # 	elif 'مسلمة' in text_all:
    # 		text='مسلمة'
    # 	elif 'مسيحية' in text_all:
    # 		text='مسيحية'
    # if name=='gender':
    # 	if 'ذكر' in text_all:
    # 		text='ذكر'
    # 	elif 'أنثى' in text_all:
    # 		text='أنثى'
    # 	elif 'انثى' in text_all:
    # 		text='أنثى'

    # if name=='a3zb':
    # 	if 'أنسة' in text_all:
    # 		text='أنسة'
    # 	elif 'متزوجة' in text_all:
    # 		text='متزوجة'
    # 	elif 'متزوج' in text_all:
    # 		text='متزوج'
    # 	elif 'أعزب' in text_all:
    # 		text='أعزب'

    return text


# Reading the data from the back side of the ID card
def back_read(source_image):

    img = Image.open(source_image)  # 1005 630
    im_gray = cv2.imread(source_image, cv2.IMREAD_GRAYSCALE)
    ret, thresh_img = cv2.threshold(im_gray, 145, 255, cv2.THRESH_BINARY)
    kernel = np.ones((2, 2), np.uint8)
    cv2.imwrite('threshold.jpg', thresh_img)
    img_binary = Image.open('threshold.jpg')
    text_all = image_to_string(img_binary, lang='ara')
    global job, job2, gender, religion, a3zb, husband
    global IDNumber

    job = crop(230, 70, 820, 140, 'job', img_binary, text_all)
    # job2 represents the place of work
    job2 = crop(230, 125, 820, 190, 'job2', img_binary, text_all)
    # gender = crop(700,180,820,260,'gender',img_binary,text_all) #we take gender from it's place in ID
    # we take the gender (Male or Female) from ID number
    gender = gen(IDNumber)
    religion = crop(480, 180, 760, 260, 'religion', img_binary, text_all)

    a3zb = crop(200, 180, 570, 260, 'a3zb', img_binary, text_all)
    # husband name in case of women
    husband = crop(200, 225, 820, 290, 'husband', img_binary, text_all)

