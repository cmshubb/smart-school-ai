from facereko_webapp import create_app
from flask_migrate import MigrateCommand, Manager
app = create_app()

manager = Manager(app)

manager.add_command('db', MigrateCommand)


if __name__ == "__main__":
    manager.run()
