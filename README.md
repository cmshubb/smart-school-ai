# Migration Tool

```
pip install Flask-Migrate
pip install Flask-Script
```

## Running the tool

To run the tool from the command line just open up your CMD and write

```
python manage.py db migrate
```

If the migration folder doesn't exist then you can create it once by running

```
python manage.py db init
```
