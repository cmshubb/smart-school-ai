from os import environ, path
from dotenv import load_dotenv

basedir = path.abspath(path.dirname(__file__))
load_dotenv(path.join(basedir, '.env'))


class Config:
    """Set Flask configuration variables from .env file."""

    # General Flask Config
    SECRET_KEY = environ.get('SECRET_KEY')
    FLASK_ENV = environ.get('FLASK_ENV')
    FLASK_APP = 'main.py'
    FLASK_DEBUG = 1

    # Database
    SQLALCHEMY_DATABASE_URI = environ.get("SQLALCHEMY_DATABASE_URI")
    SQLALCHEMY_ECHO = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # file paths config
    DATASET = path.join("./facereko_webapp/dataset")
    UPLOADS = path.join("./facereko_webapp/uploads")
    TEMP = path.join("./facereko_webapp/temp")
    LOGS = path.join("./facereko_webapp/logs")
    PLATES = path.join("./facereko_webapp/plates")
    YOLO_MODEL = path.join("/facereko_webapp/libraries/YoloModel")
    ALLOWED_IMAGE_EXTENSIONS = ["JPEG", "JPG", "PNG", "GIF"]
    # Language
    LANGUAGES = ['ar', 'en']
