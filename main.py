"""App entry point."""
from facereko_webapp import create_app
app = create_app()

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True, threaded=True)
    #app.run(use_debugger=True, use_reloader=True, passthrough_errors=True)
